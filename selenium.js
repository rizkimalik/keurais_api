const chrome = require('selenium-webdriver/chrome');
const { By, Builder } = require('selenium-webdriver');
const { response } = require('./helper');

exports.login = async function (req, res) {
    try {
        const { url_login } = req.body;

        // Inisialisasi browser Chrome
        const options = new chrome.Options();
        options.addArguments('--disable-extensions');
        options.addArguments('--disable-popup-blocking');
        options.addArguments('--disable-infobars');
        options.addArguments('--start-maximized');

        const driver = new Builder()
            .forBrowser('chrome')
            .setChromeOptions(options)
            .build();

        // Buka halaman login
        // driver.get('https://wa5161.api-wa.my.id/login');
        driver.get(url_login);
        driver.sleep(5000);

        // Isi formulir login
        driver.findElement(By.id('field-login')).sendKeys('djkelabu@gmail.com');
        driver.sleep(1000);
        driver.findElement(By.id('field-password')).sendKeys('onesender12345');
        driver.sleep(1000);
        driver.findElement(By.id('btn-login')).click();

        // Tunggu hingga halaman selesai dimuat
        // Tutup browser
        // driver.quit();
        response.ok(res, driver);
        driver.sleep(30 * 60 * 1000);
    } 
    catch (error) {
        logger('selenium/login', error.message);
        res.status(500).end();
    }
}
// login();

