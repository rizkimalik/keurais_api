const fs = require('fs');
const http = require('http');
const https = require('https');
const cors = require('cors');
const express = require('express')
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const socketServer = require('socket.io');
const swaggerUi = require('swagger-ui-express');
const swaggerSpecs = require('./swagger.json');
const port_http = process.env.PORT || 30001;
const port_https = process.env.PORT || 35000;

dotenv.config();
express.application.prefix = express.Router.prefix = function (path, configure) {
    const router = express.Router();
    this.use(path, router);
    configure(router);
    return router;
}

const credentials = {
    key: fs.readFileSync(process.env.KEY_FILE, 'utf8'),
    cert: fs.readFileSync(process.env.CRT_FILE, 'utf8')
}

const app = express();
const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);
const io = socketServer(httpsServer, {
    cors: {
        // origin: ['https://demo.keurais.com', 'https://dev.keurais.com'],
        origin: '*',
        methods: ["GET", "POST", "PUT", "DELETE"]
    }
});

//? parse & cors application/json
// app.use(cors({
//     credentials: true,
//     origin: ['https://app.mendawai.com'],
// }));
app.use(cors());
app.use(fileUpload());
app.use(bodyParser.json({ limit: '25mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '25mb' }));

//? file routes end point
app.use('/logs', express.static('./logs'));
app.use('/attachment', express.static('./' + process.env.ATTACHMENT_DIR));
app.use('/attachment_email', express.static(process.env.ATTACHMENT_EMAIL));

//? api-dosc swagger UI
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpecs));

//? routes api endpoint
const api = require('./routes/api');
api(app, io);

//? routes socket endpoint
const socket = require('./routes/socket');
socket(io);

//? routes blending
const blending = require('./routes/blending');
blending(app, io);

httpServer.listen(port_http, () => console.log(`http://localhost:${port_http}`));
httpsServer.listen(port_https, () => console.log(`https://localhost:${port_https}`));

