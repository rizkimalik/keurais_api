const channel = require('./channel_controller');
const status = require('./status_controller');
const user_level = require('./user_level_crontroller');
const category = require('./category_controller');
const categorysublv1 = require('./categorysublv1_controller');
const categorysublv2 = require('./categorysublv2_controller');
const categorysublv3 = require('./categorysublv3_controller');
const organization = require('./organization_controller');
const department = require('./department_controller');
const customer_type = require('./customer_type');
const priority_scale = require('./priority_scale');
const chat_response_template = require('./chat_response_template');

module.exports =  {
    channel,
    status,
    user_level,
    category,
    categorysublv1,
    categorysublv2,
    categorysublv3,
    organization,
    department,
    customer_type,
    priority_scale,
    chat_response_template,
}