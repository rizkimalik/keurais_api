const knex = require('../../config/db_connect');
const response = require('../../helper/json_response');

const index = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { data } = req.query;
        
        let result;
        if (data == 'active') {
            result = await knex('customer_type').where({ active: '1' }).orderBy('id', 'asc');
        }
        else {
            result = await knex('customer_type').orderBy('id', 'asc');
        }
        response.ok(res, result);
    }
    catch (error) {
        logger('customer_type/index', error);
        res.status(500).end();
    }
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { id } = req.params;
        const result = await knex('customer_type').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        logger('customer_type/show', error);
        res.status(500).end();
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            type_name,
            description,
            active,
            created_by,
            created_at = knex.fn.now(),
        } = req.body;

        await knex('customer_type')
            .insert([{
                type_name,
                description,
                active,
                created_by,
                created_at,
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        logger('customer_type/store', error);
        res.status(500).end();
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const {
            id,
            type_name,
            description,
            active,
            updated_by,
            updated_at = knex.fn.now(),
        } = req.body;

        await knex('customer_type')
            .update({
                type_name,
                description,
                active,
                updated_by,
                updated_at
            })
            .where({ id });
        response.ok(res, 'success update');
    }
    catch (error) {
        logger('customer_type/update', error);
        res.status(500).end();
    }
}

const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('customer_type').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        logger('customer_type/destroy', error);
        res.status(500).end();
    }
}

module.exports = {
    index,
    show,
    store,
    update,
    destroy,
}