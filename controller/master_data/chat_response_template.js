const knex = require('../../config/db_connect');
const response = require('../../helper/json_response');

const index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const results = await knex.raw(`
            SELECT * FROM chat_template
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from chat_template ${filtering}`);
        response.data(res, results[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, JSON.stringify(error), 'chat_response_template')
    }
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { id } = req.params;
        const result = await knex('chat_template').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, JSON.stringify(error), 'chat_response_template');
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            short_code,
            content_message,
            created_by,
            created_at = knex.fn.now(),
        } = req.body;

        await knex('chat_template')
            .insert([{
                short_code,
                content_message,
                created_by,
                created_at,
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        response.error(res, JSON.stringify(error), 'chat_response_template');
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const {
            id,
            short_code,
            content_message,
            updated_by,
            updated_at = knex.fn.now(),
        } = req.body;

        await knex('chat_template')
            .update({
                short_code,
                content_message,
                updated_by,
                updated_at
            })
            .where({ id });
        response.ok(res, 'success update');
    }
    catch (error) {
        response.error(res, JSON.stringify(error), 'chat_response_template');
    }
}

const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('chat_template').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, JSON.stringify(error), 'chat_response_template');
    }
}

module.exports = {
    index,
    show,
    store,
    update,
    destroy,
}