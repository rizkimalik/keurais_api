const knex = require('../../config/db_connect');
const response = require('../../helper/json_response');

const index = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { data } = req.query;
        
        let result;
        if (data == 'active') {
            result = await knex('channels').select('channel').groupBy('channel').where({ active: '1' });
        }
        else {
            result = await knex('channels').orderBy('id', 'asc');
        }
        response.ok(res, result);
    }
    catch (error) {
        logger('channel/index', error);
        res.status(500).end();
    }
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { id } = req.params;
        const result = await knex('channels').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        logger('channel/show', error);
        res.status(500).end();
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            channel,
            channel_type,
            description,
            active,
            created_by,
            created_at = knex.fn.now(),
        } = req.body;

        await knex('channels')
            .insert([{
                channel,
                channel_type,
                description,
                active,
                created_by,
                created_at,
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        logger('channel/store', error);
        res.status(500).end();
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const {
            id,
            channel,
            channel_type,
            description,
            active,
            updated_by,
            updated_at = knex.fn.now(),
        } = req.body;

        await knex('channels')
            .update({
                channel,
                channel_type,
                description,
                active,
                updated_by,
                updated_at
            })
            .where({ id });
        response.ok(res, 'success update');
    }
    catch (error) {
        logger('channel/update', error);
        res.status(500).end();
    }
}

const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('channels').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        logger('channel/destroy', error);
        res.status(500).end();
    }
}

const channel_type = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { channel } = req.query;
        const channel_type = await knex('channels').select('channel_type','channel').groupBy('channel_type','channel').where({ active: '1', channel });
        response.ok(res, channel_type);
    }
    catch (error) {
        logger('channel/channel_type', error);
        res.status(500).end();
    }
}


module.exports = {
    index,
    show,
    store,
    update,
    destroy,
    channel_type,
}