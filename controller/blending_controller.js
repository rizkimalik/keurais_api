'use strict';
// const knex = require('../config/db_connect');
const logger = require('../helper/logger');
const response = require('../helper/json_response');

const send_message_cust = function (req, res, io) {
    try {
        const data = req.body;
        io.to(data.agent_handle).emit('send-message-customer', data);
        response.ok(res, data);
    } catch (error) {
        logger('blending/send_message_cust', error);
    }
}

const send_message_agent = function (req, res, io) {
    try {
        const data = req.body;
        io.to(data.email).emit('send-message-agent', data);
        response.ok(res, data);
    } catch (error) {
        logger('blending/send_message_cust', error);
    }
}

const queing_chat = function (req, res, io) {
    try {
        const data = req.body;
        io.to(data.email).emit('queing', data);
        response.ok(res, data);
    } catch (error) {
        logger('blending/send_message_cust', error);
    }
}

const send_message_whatsapp = function (req, res, io) {
    try {
        const data = req.body;
        io.to(data.agent_handle).emit('return-message-whatsapp', data);
        response.ok(res, data);
    } catch (error) {
        logger('blending/send_message_whatsapp', error);
    }
}

const send_directmessage_twitter = function (req, res, io) {
    try {
        const data = req.body;
        io.to(data.agent_handle).emit('return-directmessage-twitter', data);
        response.ok(res, data);
    } catch (error) {
        logger('blending/send_directmessage_twitter', error);
    }
}

const send_email_in = function (req, res, io) {
    try {
        const data = req.body;
        io.to(data.agent_handle).emit('return-email-in', data);
        response.ok(res, data);
    } catch (error) {
        logger('blending/send_email_in', error);
    }
}

const send_facebook_messenger = function (req, res, io) {
    try {
        const data = req.body;
        io.to(data.agent_handle).emit('return-facebook-messenger', data);
        response.ok(res, data);
    } catch (error) {
        logger('blending/send_facebook_messenger', error);
    }
}

const send_facebook_feed = function (req, res, io) {
    try {
        const data = req.body;
        io.to(data.agent_handle).emit('return-facebook-feed', data);
        response.ok(res, data);
    } catch (error) {
        logger('blending/send_facebook_feed', error);
    }
}

const send_instagram_messenger = function (req, res, io) {
    try {
        const data = req.body;
        io.to(data.agent_handle).emit('return-instagram-messenger', data);
        response.ok(res, data);
    } catch (error) {
        logger('blending/send_instagram_messenger', error);
    }
}

const send_instagram_feed = function (req, res, io) {
    try {
        const data = req.body;
        io.to(data.agent_handle).emit('return-instagram-feed', data);
        response.ok(res, data);
    } catch (error) {
        logger('blending/send_instagram_feed', error);
    }
}

const send_message_telegram = function (req, res, io) {
    try {
        const data = req.body;
        io.to(data.agent_handle).emit('return-message-telegram', data);
        response.ok(res, data);
    } catch (error) {
        logger('blending/send_message_telegram', error);
    }
}

module.exports = {
    send_message_cust,
    send_message_agent,
    queing_chat,
    send_message_whatsapp,
    send_directmessage_twitter,
    send_email_in,
    send_facebook_messenger,
    send_facebook_feed,
    send_instagram_messenger,
    send_instagram_feed,
    send_message_telegram,
}
