const path = require('path');
const axios = require('axios');
const date = require('date-and-time');
const knex = require('../../config/db_connect');
const { response, logger, file_manager, random_string } = require('../../helper');
const { insert_channel_customer } = require('../customer_channel_controller');
const { sentiment_analysis } = require('../sentiment_analysis');
const { update_sent_message_status, check_channel_token } = require('./sosial_media');

const whatsapp_webhook = async function (req, res, io) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const data = req.body;
        const channel = await check_channel_token({ page_id: data.recipient_phone, channel: 'Whatsapp' });
        if (channel === '') return response.forbidden(res, 'channel not registered', 'ERROR/omnichannel/whatsapp_webhook'); //? check channel validation

        if (data.is_from_me === true) return await whatsapp_webhook_selfmessage(io, res, data);
        if (data.is_group === true) return await whatsapp_webhook_group(io, res, data);
        const now = new Date();
        const generate_chatid = date.format(now, 'YYYYMMDDHHmmSSSmmSSS');
        data.channel = 'Whatsapp';
        data.flag_to = 'customer';
        data.page_id = data.recipient_phone;
        data.customer_id = await insert_data_customer({ username: data.sender_push_name, account_id: data.sender_phone });
        const analysis = await sentiment_analysis(data.message_text);
        const chat = await knex('chats').select('chat_id', 'agent_handle')
            .where({
                user_id: data.sender_phone,
                flag_end: 'N',
                channel: 'Whatsapp',
                page_id: data.recipient_phone
            }).first();
        data.chat_id = chat ? chat.chat_id : generate_chatid;

        const customer_spam = await knex.raw(`select COUNT(customer_id) as total from customer_account_spam where channel='Whatsapp' and (customer_id='${data.customer_id}' OR user_id='${data.sender_phone}')`);
        if (customer_spam[0][0].total > 0) {
            data.agent_handle = 'SPAM';
            data.flag_end = 'Y';
            data.status_chat = 'closed';
        }
        else {
            data.agent_handle = chat ? chat.agent_handle : '';
            data.flag_end = 'N';
            data.status_chat = 'open';
        }

        const values = {
            chat_id: data.chat_id,
            user_id: data.sender_phone,
            message: data.message_text,
            message_type: data.message_type,
            name: data.sender_push_name,
            email: data.sender_phone,
            page_id: data.page_id,
            message_id: data.message_id,
            channel: data.channel,
            customer_id: data.customer_id,
            flag_to: data.flag_to,
            status_chat: data.status_chat,
            flag_end: data.flag_end,
            agent_handle: data.agent_handle,
            date_create: new Date(),
            sentiment: analysis?.sentiment,
            objective_score: analysis?.obj_score,
            positive_score: analysis?.pos_score,
            negative_score: analysis?.neg_score,
            // date_create: knex.fn.now()
            // date_create: data.message_timestamp
        }

        await knex('chats').insert([values]);

        if (data.message_type !== 'text') {
            const account = await whatsapp_token_detail({ page_id: data.page_id });

            let filesize = 0;
            let attachment_name = '';
            if (data.message_type === 'image') {
                attachment_name = data.attachment_id + '.jpg';
                // filesize = data.message_raw.imageMessage.fileLength;
            }
            else if (data.message_type === 'document') {
                const extension = path.extname(data.attachment_name);
                attachment_name = data.attachment_id + extension;
                // filesize = data.message_raw.documentMessage.fileLength;
            }
            else if (data.message_type === 'video') {
                attachment_name = data.attachment_id + '.mp4';
                // filesize = data.message_raw.videoMessage.fileLength;
            }

            const path_url = await file_manager.DownloadFileURL({
                channel: data.channel,
                file_name: attachment_name,
                file_url: account.url_api + data.attachment_url
            });
           
            values.attachment = [{
                file_name: attachment_name,
                file_origin: account.url_api + data.attachment_url,
                file_size: filesize,
                file_type: data.message_type,
                file_url: path_url
            }];
            await insert_data_attachment({
                chat_id: data.chat_id,
                channel: data.channel,
                message_id: data.message_id,
                message_type: data.message_type,
                file_origin: account.url_api + data.attachment_url,
                file_name: attachment_name,
                file_size: filesize,
                file_url: path_url
            });
        }
        if (data.agent_handle) {
            io.to(data.chat_id).to(data.agent_handle).emit('return-message-whatsapp', values);
        }

        response.ok(res, data);
    }
    catch (error) {
        logger('ERROR/whatsapp_webhook', error.message);
        res.status(500).end();
    }
}

// function non http
const whatsapp_send_messages = async function (data) {
    try {
        const account = await whatsapp_token_detail({ page_id: data.page_id });
        data.message_id = random_string(20);
        let values = '';
        let upload_status = '';

        if (data.message_type === 'text') {
            upload_status = 'true';
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                text: {
                    body: data.message
                }
            }
        }
        else if (data.message_type === 'image') {
            upload_status = await upload_attachment_file(data);
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                image: {
                    link: data.attachment[0].file_base + '/' + data.attachment[0].file_url,
                    caption: data.message
                }
            }
        }
        else if (data.message_type === 'video') {
            upload_status = await upload_attachment_file(data);
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                video: {
                    link: data.attachment[0].file_base + '/' + data.attachment[0].file_url,
                    caption: data.message
                }
            }
        }
        else if (data.message_type === 'document') {
            upload_status = await upload_attachment_file(data);
            values = {
                recipient_type: "individual",
                to: data.user_id,
                type: data.message_type,
                document: {
                    link: data.attachment[0].file_base + '/' + data.attachment[0].file_url,
                    caption: data.message
                }
            }

        }

        if (upload_status) {
            await insert_data_message(data); // save to db
            await axios({
                url: `${account.url_api}/api/v1/messages`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': account.token,
                },
                data: JSON.stringify(values),
            })
                .then(async (result) => {
                    logger('INFO/whatsapp_send_messages', result.data);
                    if (result.data.code === 200) {
                        await update_sent_message_status({ flag_sent: 1, message_id: data.message_id, reply_id: data?.reply_id });
                    } else {
                        await update_sent_message_status({ flag_sent: 2, message_id: data.message_id, reply_id: data?.reply_id });
                    }
                })
                .catch(async (error) => {
                    logger('ERROR/whatsapp_send_messages', error.message);
                    await update_sent_message_status({ flag_sent: 2, message_id: data.message_id, reply_id: data?.reply_id });
                });
        }
    }
    catch (error) {
        logger('ERROR/whatsapp_send_messages', error.message);
        await update_sent_message_status({ flag_sent: 2, message_id: data.message_id, reply_id: data?.reply_id });
    }
}

const whatsapp_group_send_messages = async function (data) {
    try {
        const account = await whatsapp_token_detail({ page_id: data.page_id });
        data.message_id = random_string(20);
        let values = '';
        let upload_status = '';

        if (data.message_type === 'text') {
            upload_status = 'true';
            values = {
                recipient_type: "group",
                to: data.user_id,
                type: data.message_type,
                text: {
                    body: data.message
                }
            }
        }
        else if (data.message_type === 'image') {
            upload_status = await upload_attachment_file(data);
            values = {
                recipient_type: "group",
                to: data.user_id,
                type: data.message_type,
                image: {
                    link: data.attachment[0].file_base + '/' + data.attachment[0].file_url,
                    caption: data.message
                }
            }
        }
        else if (data.message_type === 'document') {
            upload_status = await upload_attachment_file(data);
            values = {
                recipient_type: "group",
                to: data.user_id,
                type: data.message_type,
                document: {
                    link: data.attachment[0].file_base + '/' + data.attachment[0].file_url,
                    caption: data.message
                }
            }

        }

        if (upload_status) {
            await insert_data_message(data); // save to db
            await axios({
                url: `${account.url_api}/api/v1/messages`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': account.token,
                },
                data: JSON.stringify(values),
            })
                .then(async (result) => {
                    logger('INFO/whatsapp_group_send_messages', result.data);
                    if (result.data.code === 200) {
                        await update_sent_message_status({ flag_sent: 1, message_id: data.message_id, reply_id: data?.reply_id });
                    } else {
                        await update_sent_message_status({ flag_sent: 2, message_id: data.message_id, reply_id: data?.reply_id });
                    }
                })
                .catch(async (error) => {
                    logger('ERROR/whatsapp_group_send_messages', error.message);
                    await update_sent_message_status({ flag_sent: 2, message_id: data.message_id, reply_id: data?.reply_id });
                });
        }
    }
    catch (error) {
        logger('ERROR/whatsapp_group_send_messages', error.message);
        await update_sent_message_status({ flag_sent: 2, message_id: data.message_id, reply_id: data?.reply_id });
    }
}

const whatsapp_token_detail = async function ({ page_id }) {
    try {
        const result = await knex('sosmed_channels').where({ page_id, channel: 'Whatsapp' }).first();
        logger('INFO/whatsapp_token_detail', result);
        return result;
    }
    catch (error) {
        logger('ERROR/whatsapp_token_detail', error.message);
    }
}

const insert_data_customer = async function (data) {
    const now = new Date();
    const generate_customerid = date.format(now, 'YYMMDDHHmmSS');
    const customer = await knex('customers').select('customer_id').where({ phone_number: data.account_id }).first();
    const channel = await knex('customer_channels').select('customer_id').where({ value_channel: data.account_id }).first();
    const customer_id = customer ? customer.customer_id : generate_customerid;

    if (!customer) {
        await knex('customers')
            .insert([{
                customer_id: customer_id,
                name: data.username,
                phone_number: data.account_id,
                email: (data.account_id).indexOf('@') >= 0 ? data.account_id : null, // for whatsapp group
                source: 'Whatsapp',
                status: 'Initialize',
                created_at: knex.fn.now()
            }]);
    }
    if (!channel) {
        await insert_channel_customer({ customer_id, value_channel: data.account_id, flag_channel: 'Phone' });
    }
    return customer_id;
}

const insert_data_message = async function (data) {
    const analysis = await sentiment_analysis(data.message);
    await knex('chats')
        .insert([{
            chat_id: data.chat_id,
            user_id: data.user_id,
            customer_id: data.customer_id,
            message: data.message,
            message_type: data.message_type,
            message_id: data.message_id,
            name: data.name,
            email: data.email,
            agent_handle: data.agent_handle,
            channel: 'Whatsapp',
            flag_to: 'agent',
            status_chat: 'open',
            flag_end: 'N',
            sentiment: analysis?.sentiment,
            objective_score: analysis?.obj_score,
            positive_score: analysis?.pos_score,
            negative_score: analysis?.neg_score,
            date_create: knex.fn.now()
        }]);
}

const whatsapp_webhook_group = async function (io, res, data) {
    try {
        const now = new Date();
        const generate_chatid = date.format(now, 'YYYYMMDDHHmmSSSmmSSS');
        data.channel = 'Whatsapp_Group';
        data.flag_to = 'customer';
        data.page_id = data.recipient_phone;
        data.customer_id = await insert_data_customer({ username: data.from_group_name, account_id: data.from_group_id });
        const analysis = await sentiment_analysis(data.message_text);
        const chat = await knex('chats').select('chat_id', 'agent_handle')
            .where({
                user_id: data.from_group_id,
                flag_to: 'customer',
                flag_end: 'N',
                channel: data.channel,
                page_id: data.page_id
            }).first();
        data.chat_id = chat ? chat.chat_id : generate_chatid;

        const customer_spam = await knex.raw(`select COUNT(customer_id) as total from customer_account_spam where channel='${data.channel}' and (customer_id='${data.customer_id}' OR user_id='${data.from_group_id}')`);
        if (customer_spam[0][0].total > 0) {
            data.agent_handle = 'SPAM';
            data.flag_end = 'Y';
            data.status_chat = 'closed';
        }
        else {
            data.agent_handle = chat ? chat.agent_handle : '';
            data.flag_end = 'N';
            data.status_chat = 'open';
        }

        const values = {
            chat_id: data.chat_id,
            user_id: data.from_group_id,
            message: data.sender_push_name + ': ' + data.message_text,
            message_type: data.message_type,
            name: data.from_group_name,
            email: data.from_group_id,
            page_id: data.page_id,
            message_id: data.message_id,
            channel: data.channel,
            customer_id: data.customer_id,
            flag_to: data.flag_to,
            status_chat: data.status_chat,
            flag_end: data.flag_end,
            agent_handle: data.agent_handle,
            date_create: new Date(),
            sentiment: analysis?.sentiment,
            objective_score: analysis?.obj_score,
            positive_score: analysis?.pos_score,
            negative_score: analysis?.neg_score,
        }

        await knex('chats').insert([values]);

        if (data.message_type !== 'text') {
            const account = await whatsapp_token_detail({ page_id: data.page_id });

            let filesize = 0;
            let attachment_name = '';
            if (data.message_type === 'image') {
                attachment_name = data.attachment_id + '.jpg';
                // filesize = data.message_raw.imageMessage.fileLength;
            }
            else if (data.message_type === 'document') {
                const extension = path.extname(data.attachment_name);
                attachment_name = data.attachment_id + extension;
                // filesize = data.message_raw.documentMessage.fileLength;
            }
            else if (data.message_type === 'video') {
                attachment_name = data.attachment_id + '.mp4';
                // filesize = data.message_raw.videoMessage.fileLength;
            }

            const path_url = await file_manager.DownloadFileURL({
                channel: data.channel,
                file_name: attachment_name,
                file_url: account.url_api + data.attachment_url
            });
            values.attachment = [{
                file_name: attachment_name,
                file_origin: account.url_api + data.attachment_url,
                file_size: filesize,
                file_url: path_url
            }];
            await insert_data_attachment({
                chat_id: data.chat_id,
                channel: data.channel,
                message_id: data.message_id,
                message_type: data.message_type,
                file_origin: account.url_api + data.attachment_url,
                file_name: attachment_name,
                file_size: filesize,
                file_url: path_url
            });
        }
        if (data.agent_handle) {
            io.to(data.chat_id).to(data.agent_handle).emit('return-message-whatsapp', values);
        }

        response.ok(res, data);
    }
    catch (error) {
        logger('ERROR/whatsapp_webhook_group', error.message);
        res.status(500).end();
    }
}

const insert_data_attachment = async function (data) {
    await knex('chat_attachments')
        .insert([{
            chat_id: data.chat_id,
            message_id: data.message_id,
            file_origin: data.file_origin,
            file_name: data.file_name,
            file_type: data.message_type,
            file_url: data.file_url,
            file_size: data.file_size,
        }]);
}

const upload_attachment_file = async function (data) {
    const { chat_id, channel, message_id, message_type, attachment } = data;
    let result = '';
    //upload file attachment
    if (attachment) {
        for (let i = 0; i < attachment.length; i++) {
            let value = { chat_id, channel, message_id, message_type, attachment: attachment[i].attachment, file_origin: attachment[i].file_origin, file_name: attachment[i].file_name, file_size: attachment[i].file_size, file_url: attachment[i].file_url }
            result = await file_manager.UploadAttachment(value);
            await insert_data_attachment(value);
        }

        return result;
    }
}

const whatsapp_webhook_selfmessage = async function (io, res, data) {
    try {
        if(data.sender_push_name !== '') return '';
        const now = new Date();
        const generate_chatid = date.format(now, 'YYYYMMDDHHmmSSSmmSSS');
        data.channel = 'Whatsapp';
        data.flag_to = 'agent';
        data.page_id = data.recipient_phone;
        data.user_id = (data.to_id).split('@')[0];
        data.customer_id = await insert_data_customer({ username: data.sender_push_name, account_id: (data.to_id).split('@')[0] });
        const analysis = await sentiment_analysis(data.message_text);
        const chat = await knex('chats').select('chat_id', 'agent_handle')
            .where({
                user_id: data.user_id,
                flag_end: 'N',
                channel: data.channel,
                page_id: data.page_id
            }).first();
        data.chat_id = chat ? chat.chat_id : generate_chatid;
        data.agent_handle = chat ? chat.agent_handle : '';
        data.flag_end = 'N';
        data.status_chat = 'open';

        const values = {
            chat_id: data.chat_id,
            user_id: data.user_id,
            message: data.message_text,
            message_type: data.message_type,
            name: data.sender_push_name,
            email: data.user_id,
            page_id: data.page_id,
            message_id: data.message_id,
            channel: data.channel,
            customer_id: data.customer_id,
            flag_to: data.flag_to,
            status_chat: data.status_chat,
            flag_end: data.flag_end,
            flag_sent: 1,
            agent_handle: data.agent_handle,
            date_create: new Date(),
            sentiment: analysis?.sentiment,
            objective_score: analysis?.obj_score,
            positive_score: analysis?.pos_score,
            negative_score: analysis?.neg_score,
        }

        await knex('chats').insert([values]);

        if (data.message_type !== 'text') {
            const account = await whatsapp_token_detail({ page_id: data.page_id });

            let filesize = 0;
            let attachment_name = '';
            if (data.message_type === 'image') {
                attachment_name = data.attachment_id + '.jpg';
            }
            else if (data.message_type === 'document') {
                const extension = path.extname(data.attachment_name);
                attachment_name = data.attachment_id + extension;
            }
            else if (data.message_type === 'video') {
                attachment_name = data.attachment_id + '.mp4';
            }

            const path_url = await file_manager.DownloadFileURL({
                channel: data.channel,
                file_name: attachment_name,
                file_url: account.url_api + data.attachment_url
            });
            values.attachment = [{
                file_name: attachment_name,
                file_origin: account.url_api + data.attachment_url,
                file_size: filesize,
                file_url: path_url
            }];
            await insert_data_attachment({
                chat_id: data.chat_id,
                channel: data.channel,
                message_id: data.message_id,
                message_type: data.message_type,
                file_origin: account.url_api + data.attachment_url,
                file_name: attachment_name,
                file_size: filesize,
                file_url: path_url
            });
        }
        if (data.agent_handle) {
            io.to(data.chat_id).to(data.agent_handle).emit('return-message-whatsapp', values);
        }

        response.ok(res, data);
    }
    catch (error) {
        logger('ERROR/whatsapp_webhook_selfmessage', error.message);
        res.status(500).end();
    }
}

module.exports = {
    whatsapp_webhook,
    whatsapp_send_messages,
    whatsapp_group_send_messages,
}