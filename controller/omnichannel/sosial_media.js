'use strict';
const knex = require('../../config/db_connect');
const { logger, response } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

const update_sent_message_status = async function (data) {
    // 0,null=waiting, 1=sent, 2=failed
    await knex('chats').update({ flag_sent: data.flag_sent, reply_id: data?.reply_id }).where({ message_id: data.message_id });
}

const list_inbox_messages = async function (req, res) {
    try {
        const { agent_handle } = req.body;
        const user = await knex('users').select('user_level').where({ username: agent_handle }).first();

        let result = '';
        if (user.user_level === 'Admin' || user.user_level === 'SPV') {
            result = await knex('v_omnichannel_customer');
        } else {
            result = await knex('v_omnichannel_customer').where({ agent_handle });
        }
        response.ok(res, result);
    }
    catch (error) {
        logger('ERROR/sosmed/list_inbox_messages', error.message);
    }

}

const conversation_messages = async function (req, res) {
    try {
        const { chat_id, customer_id, skip, take } = req.body;
        const dataskip = skip ?? 0; //offset
        const datatake = take ?? 10; //limit

        await knex('chats').update({ flag_notif: '1' }).where({ chat_id, customer_id }); //flag read notif
        const chats = await knex.raw(`
            select * from chats where chat_id='${chat_id}' and customer_id='${customer_id}' and flag_end='N' order by id desc
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const conversations = chats[0];

        for (let i = 0; i < conversations.length; i++) {
            let attachment = await knex('chat_attachments').where({ chat_id: conversations[i].chat_id, message_id: conversations[i].message_id })
            conversations[i].attachment = attachment ? attachment : '';
        }
        response.ok(res, conversations);
    }
    catch (error) {
        logger('ERROR/sosmed/conversation_messages', error.message);
    }
}

const conversation_closed = async function (req, res) {
    try {
        const { chat_id, customer_id } = req.body;
        const result = await knex.raw(`UPDATE chats SET flag_end='Y' WHERE chat_id='${chat_id}' AND customer_id='${customer_id}'`);
        await knex.raw(`INSERT INTO chats_end SELECT * FROM chats WHERE flag_end='Y'`);
        await knex.raw(`DELETE FROM chats WHERE flag_end='Y'`);
        response.ok(res, result);
    }
    catch (error) {
        logger('ERROR/sosmed/conversation_closed', error.message);
    }
}

const list_inbox_feeds = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { agent_handle } = req.body;
        const user = await knex('users').select('user_level').where({ username: agent_handle }).first();

        let result = '';
        if (user.user_level === 'Admin' || user.user_level === 'SPV') {
            result = await knex('v_omnichannel_inbox_feeds').orderBy('chat_id', 'DESC');
        } else {
            result = await knex('v_omnichannel_inbox_feeds').where({ agent_handle }).orderBy('chat_id', 'DESC');
        }
        response.ok(res, result);
    }
    catch (error) {
        logger('ERROR/sosmed/list_inbox_feeds', error.message);
    }
}

const conversation_feeds = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { chat_id } = req.body;
        await knex('chats').update({ flag_notif: '1' }).where({ chat_id }); //flag read notif
        const result = await knex('chats').where({ chat_id }).orderBy('id', 'ASC');
        response.ok(res, result);
    }
    catch (error) {
        logger('ERROR/sosmed/conversation_feeds', error.message);
    }
}

const show_detail_feed = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { post_id } = req.body;
        const feed = await knex('sosmed_feeds').where({ post_id }).first();
        response.ok(res, feed);
    }
    catch (error) {
        logger('ERROR/sosmed/show_detail_feed', error.message);
    }
}

const interaction_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            interaction_id,
            channel,
            customer_id,
        } = req.body;

        let result = '';
        if (channel === 'Email') {
            result = await knex.raw(`SELECT * FROM email_mailbox WHERE email_id='${interaction_id}'`);
        }
        else if (channel === 'Call') {
            result = await knex.raw(`SELECT * FROM v_call_detail_records WHERE interaction_id='${interaction_id}'`);
        } 
        else {
            result = await knex.raw(`SELECT * FROM view_chats WHERE chat_id='${interaction_id}' AND SUBSTRING_INDEX(channel, '_', 1)='${channel}' ORDER BY id ASC`);
        }

        const interaction = result[0];
        if (channel !== 'Email' && channel !== 'Call') {
            for (let i = 0; i < interaction.length; i++) {
                let attachment = await knex('chat_attachments').where({ chat_id: interaction[i].chat_id, message_id: interaction[i].message_id })
                interaction[i].attachment = attachment ? attachment : '';
            }
        }

        response.ok(res, result[0]);
    }
    catch (error) {
        logger('ERROR/sosial_media/interaction_detail', error);
        res.status(500).end();
    }
}

const history_messages = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY chat_id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT  *, DATE_FORMAT(start_time,'%Y-%m-%d %H:%i:%s') AS start_time, DATE_FORMAT(end_time,'%Y-%m-%d %H:%i:%s') AS end_time FROM v_omnichannel_data_messages 
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from v_omnichannel_data_messages ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        logger('ERROR/sosial_media/history_messages', error);
    }
}

const insert_account_spam = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const data = req.body;
        const account = await knex('customer_account_spam').where({ customer_id: data.customer_id }).first();

        if (data && !account) {
            await knex('customer_account_spam')
                .insert([{
                    customer_id: data.customer_id,
                    customer_name: data.name,
                    user_id: data.user_id,
                    channel: data.channel,
                    created_at: knex.fn.now()
                }]);
            await knex.raw(`UPDATE chats SET flag_end='Y', agent_handle='SPAM' WHERE chat_id='${data.chat_id}' AND customer_id='${data.customer_id}'`);
        }
        response.ok(res, data);
    }
    catch (error) {
        logger('ERROR/sosial_media/insert_account_spam', error);
    }
}

const check_channel_token = async function (data) {
    try {
        const channel = await knex('sosmed_channels').where({ page_id: data.page_id, channel: data.channel }).first();
        if (channel) {
            return channel;
        } else {
            return '';
        }
    } catch (error) {
        logger('ERROR/sosial_media/check_channel_token', error);
    }
}

const sosmed_notifications = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { agent_handle } = req.body;
        const results = await knex.raw(`SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create FROM chats WHERE agent_handle='${agent_handle}' AND flag_notif<>'1' AND flag_end='N'`);
        response.ok(res, results[0]);
    }
    catch (error) {
        logger('ERROR/sosial_media/sosmed_notifications', error);
    }
}

const chat_templates = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const results = await knex('chat_template').orderBy('id', 'asc');
        response.ok(res, results);
    }
    catch (error) {
        logger('ERROR/sosial_media/chat_templates', error);
    }
}

const transfer_assign_agent = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { interaction_id, agent_handle, channel } = req.body;

        if (interaction_id) {
            if (channel === 'Email') {
                await knex('email_mailbox').where({ email_id: interaction_id }).update({ agent_handle });
                response.ok(res, interaction_id);
            }
            else if (channel === 'SosialMedia') {
                await knex('chats').where({ chat_id: interaction_id }).update({ agent_handle });
                response.ok(res, interaction_id);
            }
        }
        else {
            response.error(res, 'assign failed try again.', 'email/transfer_assign_agent');
        }
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/sosial_media/transfer_assign_agent');
    }
}

const agent_ready_toassign = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { skip, take, sort, filter, channel } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY username ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const results = await knex.raw(`
            SELECT a.*,b.department_name FROM users a
            LEFT JOIN departments b ON b.id=a.department
            WHERE a.login='1' AND a.aux='1' AND a.user_level='L1'
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total FROM users a LEFT JOIN departments b ON b.id=a.department WHERE a.login='1' AND a.aux='1' AND a.user_level='L1' ${filtering}`);
        response.data(res, results[0], total[0][0].total);
    }
    catch (error) {
        logger('ERROR/sosial_media/agent_ready_toassign', error);
    }
}
module.exports = {
    update_sent_message_status,
    list_inbox_messages,
    conversation_messages,
    conversation_closed,
    list_inbox_feeds,
    conversation_feeds,
    show_detail_feed,
    interaction_detail,
    history_messages,
    insert_account_spam,
    check_channel_token,
    sosmed_notifications,
    chat_templates,
    transfer_assign_agent,
    agent_ready_toassign,
}
