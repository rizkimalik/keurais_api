'use strict';
const knex = require('../../config/db_connect');
const axios = require('axios');
const { response, logger } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

const data_subscription_channel = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const result = await knex.raw(`
            SELECT * FROM (
                select id,page_id,page_name,channel,account_id,token,token_secret,url_api,active,created_at,updated_at from sosmed_channels
                UNION ALL
                select id,username as page_id, type as page_name,'Email' as channel,username as account_id,'' as token,'' as token_secret,'' as url_api,active,created_at,updated_at from email_account
            ) as channels
            ORDER BY id DESC
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/subscription/data_subscription_channel');
    }
}

const detail_subscription_channel = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id, channel, page_id } = req.body;

        if (channel === 'Email') {
            const result = await knex.raw(`SELECT * FROM email_account WHERE id='${id}' AND username='${page_id}' limit 1`);
            response.ok(res, result[0]);
        }
        else {
            const result = await knex.raw(`SELECT * FROM sosmed_channels WHERE id='${id}' AND page_id='${page_id}' limit 1`);
            response.ok(res, result[0]);
        }
    }
    catch (error) {
        response.error(res, error, 'ERROR/detail_subscription_channel');
    }
}

const insert_subscription_channel = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const data = req.body;
        logger('INFO/insert_subscription_channel', data);

        if (data.channel === 'Email') {
            const { username, password, host, port, tls, type, active } = data;
            await knex('email_account')
                .insert([{
                    username,
                    password,
                    host,
                    port,
                    tls,
                    type,
                    active,
                    created_at: knex.fn.now()
                }]);
            response.ok(res, data);
        }
        else {
            const { page_id, page_name, page_category, channel, account_id, url_api, token, token_secret, active } = data;
            await knex('sosmed_channels')
                .insert([{
                    page_id,
                    page_name,
                    page_category,
                    channel,
                    account_id,
                    url_api,
                    token,
                    token_secret,
                    active,
                    created_at: knex.fn.now()
                }]);
            response.ok(res, data);
        }

    }
    catch (error) {
        response.error(res, error, 'ERROR/insert_subscription_channel');
    }
}

const update_subscription_channel = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const data = req.body;
        logger('INFO/update_subscription_channel', data);

        if (data.channel === 'Email') {
            const { id, username, password, host, port, tls, type, active } = data;
            await knex('email_account')
                .update({
                    username,
                    password,
                    host,
                    port,
                    tls,
                    type,
                    active,
                    updated_at: knex.fn.now()
                })
                .where({ id });
        }
        else {
            const { id, page_id, page_name, channel, account_id, url_api, token, token_secret, active } = data;
            await knex('sosmed_channels')
                .update({
                    page_id,
                    page_name,
                    channel,
                    account_id,
                    url_api,
                    token,
                    token_secret,
                    active,
                    updated_at: knex.fn.now()
                })
                .where({ id });
        }
        response.ok(res, data);
    }
    catch (error) {
        response.error(res, error, 'ERROR/update_subscription_channel');
    }
}

const delete_subscription_channel = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const data = req.body;

        if (data.channel === 'Email') {
            const result = await knex('email_account').where({ id: data.id }).del();
            response.ok(res, result);
        }
        else {
            const result = await knex('sosmed_channels').where({ id: data.id }).del();
            response.ok(res, result);
        }

    }
    catch (error) {
        response.error(res, error, 'ERROR/delete_subscription_channel');
    }
}

const data_assigned_department = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, page_id } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT * FROM (
                SELECT a.*,b.department_name,c.page_name FROM channel_department a
                LEFT JOIN departments b ON b.id=a.department_id
                LEFT JOIN sosmed_channels c ON c.page_id=a.page_id
            ) as assigned_department
            WHERE page_id='${page_id}'
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total FROM (
                SELECT a.*,b.department_name,c.page_name FROM channel_department a
                LEFT JOIN departments b ON b.id=a.department_id
                LEFT JOIN sosmed_channels c ON c.page_id=a.page_id
            ) as assigned_department
            WHERE page_id='${page_id}'
            ${filtering}
        `);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.data(res, error.message, 'ERROR/data_assigned_department');
    }
}

const insert_assign_department = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { page_id, department_id } = req.body;
        await knex('channel_department')
            .insert({
                page_id,
                department_id,
                created_at: knex.fn.now()
            });
        response.ok(res, req.body);
    }
    catch (error) {
        response.error(res, error, 'ERROR/insert_assign_department');
    }
}

const delete_assigned_department = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.body;
        const result = await knex('channel_department').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'ERROR/delete_assigned_department');
    }
}

const whatsapp_scan_qrcode = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { host, token } = req.body;

        await axios({
            url: `${host}/api/v1/remote-scan`,
            method: 'POST',
            maxBodyLength: Infinity,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        })
            .then(async ({ data }) => {
                logger('INFO/whatsapp_scan_qrcode', data);
                if (data.success === true) {
                    await axios({
                        url: `${host}/api/v1/remote-scan/${data.data.request_id}`,
                        method: 'GET',
                        maxBodyLength: Infinity,
                        headers: {
                            'Authorization': 'Bearer ' + token,
                        }
                    })
                        .then(async (result) => {
                            // result.data.login_code
                            await knex('sosmed_channels').update({ active: 1 }).where({ token });
                            logger('INFO/whatsapp_scan_qrcode', result.data);
                            response.ok(res, result.data);
                        })
                        .catch((error) => {
                            response.error(res, error.message, 'ERROR/whatsapp_scan_qrcode');
                        });
                }
                else {
                    response.error(res, error.message, 'ERROR/whatsapp_scan_qrcode');
                }
            })
            .catch((error) => {
                response.error(res, error.message, 'ERROR/whatsapp_scan_qrcode');
            });
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/whatsapp_scan_qrcode');
    }
}

const selenium_autologin = async function (req, res) {
    /* const browser = await puppeteer.launch({ headless: false }); // Launch a non-headless browser for debugging
    const page = await browser.newPage();

    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { url_api } = req.body;
        // Navigate to the login page

        await page.goto(url_api);

        // Fill in the username and password fields
        await page.type('#field-login', 'djkelabu@gmail.com');
        await page.type('#field-password', 'onesender12345');

        // Click the login button (replace with the actual selector for your login button)
        await page.click('#btn-login');

        // Wait for login to complete (you can add more specific conditions)
        await page.waitForNavigation();

        // Your login is complete, you can add further actions here if needed
        // Close the browser
        // await browser.close();
        response.ok(res, url_api);
    }
    catch (error) {
        logger('ERROR/selenium_autologin', error.message);
        await browser.close(); // Ensure the browser is closed on error
    } */
}

module.exports = {
    data_subscription_channel,
    detail_subscription_channel,
    insert_subscription_channel,
    update_subscription_channel,
    delete_subscription_channel,
    data_assigned_department,
    insert_assign_department,
    delete_assigned_department,
    whatsapp_scan_qrcode,
    selenium_autologin,
}