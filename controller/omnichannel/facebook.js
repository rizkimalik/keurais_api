const knex = require('../../config/db_connect');
const date = require('date-and-time');
const axios = require('axios');
const FormData = require('form-data');
const { logger, response, random_string, file_manager } = require('../../helper');
const { insert_channel_customer } = require('../customer_channel_controller');
const { sentiment_analysis } = require('../sentiment_analysis');
const { update_sent_message_status, check_channel_token } = require('./sosial_media');


const facebook_get_token = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const data = req.body;

        for (let i = 0; i < data.length; i++) {
            const check = await knex('sosmed_channels')
                .count('page_id as jml')
                .where({ account_id: data[i].id, channel: 'Facebook' })
                .first();

            if (check.jml > 0) {
                await knex('sosmed_channels')
                    .where({ account_id: data[i].id, channel: 'Facebook' })
                    .update({
                        page_id: data[i].id,
                        page_name: data[i].name,
                        page_category: data[i].category,
                        channel: 'Facebook',
                        // account_id: data[i].id,
                        token: data[i].access_token,
                        token_secret: '',
                        user_secret: '',
                        url_api: data[i]?.url_api,
                        updated_at: knex.fn.now(),
                    })
            }
            else {
                await knex('sosmed_channels')
                    .insert({
                        page_id: data[i].id,
                        page_name: data[i].name,
                        page_category: data[i].category,
                        channel: 'Facebook',
                        account_id: data[i].id,
                        token: data[i].access_token,
                        token_secret: '',
                        user_secret: '',
                        url_api: data[i]?.url_api,
                        created_at: knex.fn.now(),
                    })
            }
        }
        res.json({
            status: 'success',
            total_data: data.length
        });
        res.end();
    }
    catch (error) {
        logger('ERROR/omnichannel/facebook_get_token', error.message);
        res.status(500).end();
    }
}

const facebook_token_detail = async function ({ page_id }) {
    try {
        const result = await knex('sosmed_channels').where({ page_id, channel: 'Facebook' }).first();
        logger('INFO/omnichannel/facebook_token_detail', result);
        return result;
    }
    catch (error) {
        logger('ERROR/omnichannel/facebook_token_detail', error.message);
    }
}

const facebook_get_messenger = async function (req, res, io) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const data = req.body;
        const messaging = data.entry[0].messaging[0];
        if (data.entry[0].id === messaging.sender.id) return res.status(204).end(); //? except self message
        const channel = await check_channel_token({ page_id: data.entry[0].id, channel: 'Facebook' });
        if (channel === '') return response.forbidden(res, 'channel not registered', 'ERROR/omnichannel/facebook_get_messenger'); //? check channel validation

        const profile = await facebook_profile_detail({ page_id: data.entry[0].id, sender_id: messaging.sender.id });
        const generate_chatid = date.format(new Date(), 'YYYYMMDDHHmmSSSmmSSS');
        const chat = await knex('chats').select('chat_id', 'agent_handle')
            .where({
                user_id: messaging.sender.id,
                flag_to: 'customer',
                flag_end: 'N',
                channel: 'Facebook_Messenger',
                page_id: data.entry[0].id
            }).first();
        const analysis = await sentiment_analysis(messaging.message.text);

        data.channel = 'Facebook_Messenger';
        data.flag_to = 'customer';
        data.page_id = data.entry[0].id;
        messaging.name = profile.name;
        data.customer_id = await insert_data_customer({ username: profile.username, account_id: messaging.sender.id });
        data.chat_id = chat ? chat.chat_id : generate_chatid;
        data.message_type = messaging.message.attachments ? messaging.message.attachments[0].type : 'text';

        const customer_spam = await knex.raw(`select COUNT(customer_id) as total from customer_account_spam where channel='Facebook_Messenger' and (customer_id='${data.customer_id}' OR user_id='${messaging.sender.id}')`);
        if (customer_spam[0][0].total > 0) {
            data.agent_handle = 'SPAM';
            data.flag_end = 'Y';
            data.status_chat = 'closed';
        }
        else {
            data.agent_handle = chat ? chat.agent_handle : '';
            data.flag_end = 'N';
            data.status_chat = 'open';
        }

        const values = {
            chat_id: data.chat_id,
            user_id: messaging.sender.id,
            message: messaging.message.text,
            message_type: data.message_type,
            name: messaging.name,
            email: messaging.sender.id + '@facebook.com',
            message_id: messaging.message.mid,
            channel: data.channel,
            customer_id: data.customer_id,
            flag_to: data.flag_to,
            status_chat: data.status_chat,
            flag_end: data.flag_end,
            agent_handle: data.agent_handle,
            page_id: data.page_id,
            date_create: new Date(),
            sentiment: analysis?.sentiment,
            objective_score: analysis?.obj_score,
            positive_score: analysis?.pos_score,
            negative_score: analysis?.neg_score,
        }

        await knex('chats').insert([values]);
        if (data.agent_handle) {
            io.to(data.chat_id).to(data.agent_handle).emit('return-facebook-messenger', values);
        }
        if (data.message_type !== 'text') {
            // const path_url = await file_manager.DownloadFileURL(data);
            // data.url = path_url;
            // await insert_data_attachment(data);
        }

        response.ok(res, data);
    }
    catch (error) {
        logger('ERROR/omnichannel/facebook_get_messenger', error.message);
    }
}

const facebook_post_messenger = async function (value) {
    try {
        const account = await facebook_token_detail({ page_id: value.page_id });
        let data = new FormData();
        data.append('pagetoken', account.token);
        data.append('pageid', value.page_id);
        data.append('sender_id', value.user_id);
        data.append('message', value.message);
        value.message_id = random_string(20);

        await insert_message_agent(value);
        await axios({
            method: 'post',
            url: `${account.url_api}/sosial/facebook/facebook/messages_replay`,
            headers: {
                ...data.getHeaders()
            },
            data: data
        })
            .then(async (result) => {
                // value.reply_id = result.data.message_id
                logger('INFO/omnichannel/facebook_post_messenger', result.data);
                await update_sent_message_status({ flag_sent: 1, message_id: value.message_id, reply_id: value?.reply_id });
            })
            .catch(async (error) => {
                logger('ERROR/omnichannel/facebook_post_messenger', error.message);
                await update_sent_message_status({ flag_sent: 2, message_id: value.message_id, reply_id: value?.reply_id });
            });
    }
    catch (error) {
        logger('ERROR/omnichannel/facebook_post_messenger', error.message);
        await update_sent_message_status({ flag_sent: 2, message_id: value.message_id, reply_id: value?.reply_id });
    }
}

const facebook_get_feed = async function (req, res, io) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const data = req.body;
        const feed = data.entry[0].changes[0].value;
        if (data.entry[0].id === feed.from.id) return res.status(204).end(); //? except self message
        const channel = await check_channel_token({ page_id: data.entry[0].id, channel: 'Facebook' });
        if (channel === '') return response.forbidden(res, 'channel not registered', 'ERROR/omnichannel/facebook_get_feed'); //? check channel validation

        const generate_chatid = date.format(new Date(), 'YYYYMMDDHHmmSSSmmSSS');
        const interaction = await knex('chats').select('chat_id', 'agent_handle')
            .where({
                user_id: feed.from.id,
                flag_to: 'customer',
                flag_end: 'N',
                channel: 'Facebook_Feed',
                page_id: data.entry[0].id,
                post_id: feed.post_id
            }).first();
        const analysis = await sentiment_analysis(feed.text);

        data.channel = 'Facebook_Feed';
        data.flag_to = 'customer';
        data.page_id = data.entry[0].id;
        data.post_id = feed.post_id;
        data.comment_id = feed.comment_id;
        data.customer_id = await insert_data_customer({ username: feed.from.name, account_id: feed.from.id });
        data.chat_id = interaction ? interaction.chat_id : generate_chatid;
        data.message_type = feed.photo ? 'image' : 'text';

        const customer_spam = await knex.raw(`select COUNT(customer_id) as total from customer_account_spam where channel='Facebook_Feed' and (customer_id='${data.customer_id}' OR user_id='${feed.from.id}')`);
        if (customer_spam[0][0].total > 0) {
            data.agent_handle = 'SPAM';
            data.flag_end = 'Y';
            data.status_chat = 'closed';
        }
        else {
            data.agent_handle = interaction ? interaction.agent_handle : '';
            data.flag_end = 'N';
            data.status_chat = 'open';
        }

        const values = {
            chat_id: data.chat_id,
            user_id: feed.from.id,
            message: feed.message,
            message_type: data.message_type,
            name: feed.from.name,
            email: feed.from.id + '@facebook.com',
            message_id: feed.comment_id,
            channel: data.channel,
            customer_id: data.customer_id,
            flag_to: data.flag_to,
            status_chat: data.status_chat,
            flag_end: data.flag_end,
            agent_handle: data.agent_handle,
            page_id: data.page_id,
            post_id: data.post_id,
            comment_id: data.comment_id,
            date_create: new Date(),
            sentiment: analysis?.sentiment,
            objective_score: analysis?.obj_score,
            positive_score: analysis?.pos_score,
            negative_score: analysis?.neg_score,
        }

        await knex('chats').insert([values]);
        await facebook_feed_detail(values); // parent feed detail
        if (data.agent_handle) {
            io.to(data.chat_id).to(data.agent_handle).emit('return-facebook-feed', values);
        }
        if (data.message_type !== 'text') {
            // await insert_data_attachment(data);
        }

        response.ok(res, data);
    }
    catch (error) {
        logger('ERROR/omnichannel/facebook_get_feed', error.message);
    }
}

const facebook_post_feed = async function (value) {
    try {
        const account = await facebook_token_detail({ page_id: value.page_id });
        let data = new FormData();
        data.append('pagetoken', account.token);
        data.append('comment_id', value.comment_id);
        data.append('message', value.message);
        value.message_id = random_string(20);

        await insert_message_agent(value);
        await axios({
            method: 'post',
            url: `${account.url_api}/sosial/facebook/facebook/replaycomment`,
            headers: {
                ...data.getHeaders()
            },
            data: data
        })
            .then(async (result) => {
                logger('INFO/omnichannel/facebook_post_feed', result.data);
                value.reply_id = result.data.data.id;
                await update_sent_message_status({ flag_sent: 1, message_id: value.message_id, reply_id: value?.reply_id });
            })
            .catch(async (error) => {
                logger('ERROR/omnichannel/facebook_post_feed', error.message);
                await update_sent_message_status({ flag_sent: 2, message_id: value.message_id, reply_id: value?.reply_id });
            });
    }
    catch (error) {
        logger('ERROR/omnichannel/facebook_post_feed', error.message);
        await update_sent_message_status({ flag_sent: 2, message_id: value.message_id, reply_id: value?.reply_id });
    }
}

const facebook_profile_detail = async function (value) {
    try {
        const account = await facebook_token_detail({ page_id: value.page_id });
        let data = new FormData();
        data.append('pagetoken', account.token);
        data.append('profileid', value.sender_id);

        let response = '';
        await axios({
            method: 'post',
            url: `${account.url_api}/sosial/facebook/facebook/getprofile`,
            headers: {
                ...data.getHeaders()
            },
            data: data
        })
            .then(function (result) {
                // console.log(result);
                response = result.data.data;
            })
            .catch(function (error) {
                console.log(error.message);
                logger('ERROR/omnichannel/facebook_profile_detail', error.message);
            });

        return response;
    }
    catch (error) {
        logger('ERROR/omnichannel/facebook_profile_detail', error.message);
    }
}

const insert_data_customer = async function (data) {
    const now = new Date();
    const generate_customerid = date.format(now, 'YYMMDDHHmmSS');
    const customer = await knex('customers').select('customer_id').where({ account_id: data.account_id, source: 'Facebook' }).first();
    const channel = await knex('customer_channels').select('customer_id').where({ value_channel: data.account_id, flag_channel: 'Facebook' }).first();
    const customer_id = customer ? customer.customer_id : generate_customerid;

    if (!customer) {
        await knex('customers')
            .insert([{
                customer_id: customer_id,
                name: data.username,
                email: data.account_id + '@facebook.com',
                account_id: data.account_id,
                source: 'Facebook',
                status: 'Initialize',
                created_at: knex.fn.now()
            }]);
    }
    if (!channel) {
        await insert_channel_customer({
            customer_id: customer_id,
            value_channel: data.account_id,
            flag_channel: 'Facebook'
        });
    }
    return customer_id;
}

const insert_message_agent = async function (data) {
    const analysis = await sentiment_analysis(data.message);

    await knex('chats')
        .insert([{
            chat_id: data.chat_id,
            user_id: data.user_id,
            customer_id: data.customer_id,
            message: data.message,
            message_type: data.message_type,
            message_id: data.message_id,
            name: data.name,
            email: data.email,
            agent_handle: data.agent_handle,
            page_id: data.page_id,
            post_id: data?.post_id,
            comment_id: data?.comment_id,
            channel: data.channel,
            flag_to: 'agent',
            status_chat: 'open',
            flag_end: 'N',
            sentiment: analysis?.sentiment,
            objective_score: analysis?.obj_score,
            positive_score: analysis?.pos_score,
            negative_score: analysis?.neg_score,
            date_create: knex.fn.now()
        }]);
}

const facebook_feed_detail = async function (value) {
    try {
        const account = await facebook_token_detail({ page_id: value.page_id });
        let data = new FormData();
        data.append('pagetoken', account.token);
        data.append('post_id', value.post_id);

        let response = '';
        await axios({
            method: 'post',
            maxBodyLength: Infinity,
            url: `${account.url_api}/sosial/facebook/facebook/feedDetail`,
            headers: {
                ...data.getHeaders()
            },
            data: data
        })
            .then(async (result) => {
                logger('INFO/omnichannel/facebook_feed_detail', result.data);
                response = result.data.data;
                const channel = await knex('sosmed_feeds').where({ post_id: value.post_id, channel: 'Facebook_Feed' }).first();
                if (!channel) {
                    let media_name, media_type, media_url = '';
                    if (result.data.data.attachments[0].type === 'photo') {
                        media_type = 'IMAGE';
                        media_name = value.post_id + '.jpg';
                        media_url = result.data.data.attachments[0].media.image.src;
                    }
                    else if (result.data.data.attachments[0].type === 'profile_media') {
                        media_type = 'IMAGE';
                        media_name = value.post_id + '.jpg';
                        media_url = result.data.data.attachments[0].media.image.src;
                    }
                    else if (result.data.data.attachments[0].type === 'cover_photo') {
                        media_type = 'IMAGE';
                        media_name = value.post_id + '.jpg';
                        media_url = result.data.data.attachments[0].media.image.src;
                    }
                    else if (result.data.data.attachments[0].type === 'video_inline') {
                        media_type = 'VIDEO';
                        media_name = value.post_id + '.mp4';
                        media_url = result.data.data.attachments[0].media.source;
                    }

                    const path_url = await file_manager.DownloadFileURL({
                        channel: 'Facebook_Feed',
                        file_name: media_name,
                        file_url: media_url
                    });

                    await knex('sosmed_feeds')
                        .insert([{
                            post_id: value.post_id,
                            page_id: value.page_id,
                            page_name: result.data.data.from.name,
                            caption: result.data.data?.message,
                            channel: 'Facebook_Feed',
                            permalink_url: result.data.data.permalink_url,
                            media_url: path_url,
                            media_type: media_type,
                            created_at: new Date(result.data.data.created_time.date),
                            // created_at: knex.fn.now()
                        }]);
                }
            })
            .catch((error) => {
                logger('ERROR/omnichannel/facebook_feed_detail', error.message);
            });

        return response;
    }
    catch (error) {
        logger('ERROR/omnichannel/facebook_feed_detail', error.message);
    }
}

module.exports = {
    facebook_get_token,
    facebook_get_messenger,
    facebook_post_messenger,
    facebook_get_feed,
    facebook_post_feed,
}