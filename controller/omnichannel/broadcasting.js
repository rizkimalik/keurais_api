const axios = require('axios');
const readXlsxFile = require('read-excel-file/node')
const knex = require('../../config/db_connect');
const { response, logger, random_string } = require('../../helper');

exports.whatsapp_broadcast_send = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const excel_file = req.files?.excel_file;
        const data = req.body;
        let contacts, values = [];
        data.broadcast_id = random_string(20);

        const account = await whatsapp_token_detail({ page_id: data.page_id });
        // const template = await broadcast_template({ template_id: data.template_id });

        if (data.source === 'contact') {
            contacts = await broadcast_contact_list({ group_contact: data.group_contact });
        }
        else if (data.source === 'manual') {
            contacts = [{ phone_number: data.phone_number, contact_name: data.contact_name }];
            await insert_data_contact({ phone_number: data.phone_number, contact_name: data.contact_name });
        }
        else if (data.source === 'excel') {
            // Read Excel file
            await readXlsxFile(Buffer.from(excel_file.data, 'utf-8')).then((rows) => {
                let data_row = [];
                for (let i = 0; i < rows.length; i++) {
                    data_row.push({
                        phone_number: rows[i][0],
                        contact_name: rows[i][1]
                    });
                    insert_data_contact({ phone_number: rows[i][0], contact_name: rows[i][1] });
                }
                contacts = data_row;
            });
        }

        for (let i = 0; i < contacts.length; i++) {
            if (data.message_type === 'text') {
                values.push({
                    recipient_type: "individual",
                    to: contacts[i].phone_number,
                    type: "text",
                    text: {
                        body: data.message
                    }
                });
            }
            else if (data.message_type === 'image') {
                values.push({
                    recipient_type: "individual",
                    to: contacts[i].phone_number,
                    type: "image",
                    image: {
                        link: data.link,
                        caption: data.message,
                    }
                });
            }

            await broadcast_data_insert({
                broadcast_id: data.broadcast_id,
                group_contact: data?.group_contact,
                from_number: data.page_id,
                to_number: contacts[i].phone_number,
                broadcast_title: data.broadcast_title,
                message: data.message,
                message_type: data.message_type,
                schedule_at: data?.schedule_at?.replace('T',' '),
            });
        }

        // action send at the time
        if (data.action === 'send') {
            await axios({
                url: `${account.url_api}/api/v1/messages`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': account.token,
                },
                data: JSON.stringify(values),
            })
                .then(async (result) => {
                    logger('INFO/broadcasting/whatsapp_broadcast_send', result.data);
                    await broadcast_data_updated({ broadcast_id: data.broadcast_id, status: 'Success' })
                    response.ok(res, values);
                })
                .catch(async (error) => {
                    await broadcast_data_updated({ broadcast_id: data.broadcast_id, status: 'Failed' });
                    response.error(res, error.message, 'ERROR/broadcasting/whatsapp_broadcast_send');
                });
        }
        else {
            // action with schedule, add queue datetime
            response.ok(res, values);
        }

    }
    catch (error) {
        response.error(res, error.message, 'ERROR/broadcasting/whatsapp_broadcast_send');
    }
}

exports.whatsapp_channel_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const result = await knex('sosmed_channels').where({ channel: 'Whatsapp' });
        response.ok(res, result);
    }
    catch (error) {
        logger('ERROR/broadcasting/whatsapp_channel_list', error.message);
    }
}

exports.upload_excel_file = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const excel_file = req.files?.excel_file;

        // Read Excel file
        readXlsxFile(Buffer.from(excel_file.data)).then((rows) => {
            let data_row = [];
            for (let i = 0; i < rows.length; i++) {
                data_row.push({
                    no: rows[i][0],
                    name: rows[i][1]
                });
            }
            response.ok(res, data_row);
        });
    }
    catch (error) {
        logger('ERROR/upload_excel_file', error.message);
    }
}

// Non HTTP Function
const whatsapp_token_detail = async function ({ page_id }) {
    try {
        const result = await knex('sosmed_channels').where({ page_id, channel: 'Whatsapp' }).first();
        return result;
    }
    catch (error) {
        logger('ERROR/broadcasting/whatsapp_token_detail', error.message);
    }
}

const broadcast_contact_list = async function ({ group_contact }) {
    try {
        const result = await knex('broadcast_contact').where({ group_contact });
        return result;
    }
    catch (error) {
        logger('ERROR/broadcasting/broadcast_contact_list', error.message);
    }
}

const broadcast_data_insert = async function ({ broadcast_id, group_contact, from_number, to_number, broadcast_title, message, message_type, schedule_at }) {
    try {
        await knex('broadcast_whatsapp')
            .insert({
                broadcast_id,
                group_contact,
                from_number,
                to_number,
                broadcast_title,
                message,
                message_type,
                status: 'Queue',
                schedule_at,
                created_at: knex.fn.now(),
            });

        return 'success broadcast_data_insert'
    }
    catch (error) {
        logger('ERROR/broadcasting/broadcast_data_insert', error.message);
    }
}

const broadcast_data_updated = async function ({ broadcast_id, status }) {
    try {
        await knex('broadcast_whatsapp')
            .update({
                status,
                sent_at: knex.fn.now(),
            })
            .where({ broadcast_id });

        return 'success broadcast_data_updated'
    }
    catch (error) {
        logger('ERROR/broadcasting/broadcast_data_updated', error.message);
    }
}

const insert_data_contact = async function ({ phone_number, contact_name }) {
    try {
        const result = await knex('broadcast_contact').where({ phone_number }).first();
        if (!result) {
            await knex('broadcast_contact')
                .insert({
                    phone_number,
                    contact_name,
                    created_at: knex.fn.now(),
                });
        }
        return 'success insert_data_contact'
    }
    catch (error) {
        logger('ERROR/broadcasting/insert_data_contact', error.message);
    }
}