const facebook = require('./facebook');
const twitter = require('./twitter');
const instagram = require('./instagram');
const chat = require('./chat');
const email = require('./email');
const whatsapp = require('./whatsapp');
const whatsapp_business = require('./whatsapp_business');
const broadcasting = require('./broadcasting');
const call = require('./call');
const sosial_media = require('./sosial_media');
const pabx = require('./pabx');
const subcription = require('./subcription_channel');
const telegram = require('./telegram');

module.exports =  {
    facebook,
    twitter,
    instagram,
    chat,
    email,
    whatsapp,
    whatsapp_business,
    broadcasting,
    call,
    sosial_media,
    pabx,
    subcription,
    telegram,
}