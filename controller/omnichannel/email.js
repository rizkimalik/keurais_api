"use strict";
const date = require('date-and-time');
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const { response, random_string } = require('../../helper');
const { insert_channel_customer } = require('../customer_channel_controller');

const email_accounts = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { action } = req.query;

        let result = '';
        if (action === 'inbound') {
            result = await knex('email_account').where({ type: 'Inbound', active: 1 });
        }
        else if (action === 'outbound') {
            result = await knex('email_account').where({ type: 'Outbound', active: 1 });
        }
        else {
            result = await knex('email_account'); //? all data
        }
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/email/email_accounts');
    }
}

const data_email_inbox = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, agent_handle, user_level } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let params = '';
        if (user_level === 'L1') {
            params = `AND agent_handle='${agent_handle}'`;
        }
        else {
            params = '';
        }

        const result = await knex.raw(`
            SELECT id,email_id,interaction_id,eto,efrom,ecc,esubject,ticket_number,agent_handle,date_email,date_receive,date_blending
            FROM email_mailbox WHERE status is null AND (ticket_number is null OR ticket_number='') ${params}
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from email_mailbox WHERE status is null AND (ticket_number is null OR ticket_number='') ${params} ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/email/data_email_inbox');
    }
}

const data_email_sent = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT id,email_id,interaction_id,eto,efrom,ecc,esubject,ticket_number,agent_handle,date_email,date_sent,sent FROM email_out 
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from email_out ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/email/data_email_sent');
    }
}

const data_email_history = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, agent_handle, user_level } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let params = '';
        if (user_level === 'L1') {
            params = `AND agent_handle='${agent_handle}'`;
        }
        else {
            params = '';
        }

        const result = await knex.raw(`
            SELECT id,email_id,interaction_id,eto,efrom,ecc,esubject,ticket_number,agent_handle,date_email,date_receive,date_blending
            FROM email_mailbox WHERE status is null AND ticket_number<>'' ${params}
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from email_mailbox WHERE status is null AND ticket_number<>'' ${params} ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/email/data_email_history');

    }
}

const data_email_interaction = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { ticket_number } = req.body;
        const result = await knex.raw(`SELECT * FROM view_email WHERE ticket_number='${ticket_number}' order by date_email DESC`);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/email/data_email_interaction');

    }
}

const data_email_spam = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, agent_handle, user_level } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let params = '';
        if (user_level === 'L1') {
            params = `AND agent_handle='${agent_handle}'`;
        }
        else {
            params = '';
        }

        const result = await knex.raw(`
            SELECT id,email_id,interaction_id,eto,efrom,ecc,esubject,ticket_number,agent_handle,date_email,date_receive,date_blending
            FROM email_mailbox WHERE status='Spam' ${params}
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from email_mailbox WHERE status='Spam' ${params} ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/email/data_email_spam');

    }
}

const email_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { email_id } = req.body;
        const result = await knex('view_email').where({ email_id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/email/email_detail');

    }
}

const insert_email_spam = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { email_id } = req.body;

        if (email_id) {
            await knex('email_mailbox').where({ email_id }).update({ status: 'Spam' });
            response.ok(res, email_id);
        }
        else {
            response.error(res, 'insert failed try again.', 'ERROR/email/insert_email_spam');
        }
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/email/insert_email_spam');
    }
}

const update_email_unspam = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { email_id } = req.body;

        if (email_id) {
            await knex('email_mailbox').where({ email_id }).update({ status: null });
            response.ok(res, email_id);
        }
        else {
            response.error(res, 'insert failed try again.', 'ERROR/email/update_email_unspam');
        }
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/email/update_email_unspam');
    }
}

const insert_email_out = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const email_id = random_string(20);
        const {
            reply_id,
            eto,
            efrom,
            ecc,
            esubject,
            ebody_html,
            ticket_number,
            agent_handle,
            status,
        } = req.body;

        if (email_id && eto && efrom) {
            let email_body_html = '';
            if (reply_id) {
                const detail = await knex('view_email').select('ebody_html').where({ email_id: reply_id }).first();
                email_body_html = ebody_html + '<hr />' + detail.ebody_html; // ebody reply email
            }
            else{
                email_body_html = ebody_html; // ebody compose email
            }

            await knex('email_out')
                .insert([{
                    email_id,
                    eto,
                    efrom,
                    ecc,
                    esubject,
                    ebody_html: email_body_html,
                    date_email: new Date(),
                    ticket_number,
                    agent_handle,
                    direction: 'OUT',
                    status,
                }]);
            response.ok(res, email_id);
        }
        else {
            response.error(res, 'insert failed try again.', 'ERROR/email/insert_email_out');
        }
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/email/insert_email_out');
    }
}

const insert_data_customer = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const data = req.body;
        const now = new Date();
        const generate_customerid = date.format(now, 'YYMMDDHHmmSS');
        const customer = await knex('customers').select('customer_id').where({ email: data.email }).first();
        const channel = await knex('customer_channels').select('customer_id').where({ value_channel: data.email }).first();
        const customer_id = customer ? customer.customer_id : generate_customerid;

        if (!customer) {
            await knex('customers')
                .insert([{
                    customer_id: customer_id,
                    name: (data.email).split('@')[0],
                    email: data.email,
                    source: 'Email',
                    status: 'Initialize',
                    created_at: knex.fn.now()
                }]);
        }
        if (!channel) {
            await insert_channel_customer({ customer_id, value_channel: data.email, flag_channel: 'Email' });
        }

        response.ok(res, customer_id);
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/email/insert_data_customer');

    }
}


module.exports = {
    email_accounts,
    data_email_inbox,
    data_email_sent,
    data_email_history,
    data_email_interaction,
    data_email_spam,
    insert_email_spam,
    update_email_unspam,
    insert_email_out,
    insert_data_customer,
    email_detail,
}