'use strict';
const { facebook_post_messenger, facebook_post_feed } = require("../omnichannel/facebook");

module.exports = function (socket) {
    socket.on('send-facebook-messenger', (data) => {
        facebook_post_messenger(data);
        socket.to(data.chat_id).to(data.agent_handle).emit('return-facebook-messenger', data);
    });
    
    socket.on('return-facebook-messenger', (data) => {
        socket.to(data.chat_id).to(data.agent_handle).emit('return-facebook-messenger', data); //notused
    });
    
    socket.on('send-facebook-feed', (data) => {
        facebook_post_feed(data);
        socket.to(data.chat_id).to(data.agent_handle).emit('return-facebook-feed', data);
    });
    
    socket.on('return-facebook-feed', (data) => {
        socket.to(data.chat_id).to(data.agent_handle).emit('return-facebook-feed', data); //notused
    });

}