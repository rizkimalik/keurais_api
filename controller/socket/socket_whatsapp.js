'use strict';
const { whatsapp_send_messages, whatsapp_group_send_messages } = require("../omnichannel/whatsapp");
const { whatsapp_business_send_messages } = require("../omnichannel/whatsapp_business");

module.exports = function (socket) {
    socket.on('send-message-whatsapp', async (data) => {
        if (data.channel === 'Whatsapp_Business') {
            await whatsapp_business_send_messages(data);
        }
        else if (data.channel === 'Whatsapp_Group') {
            await whatsapp_group_send_messages(data);
        }
        else {
            await whatsapp_send_messages(data);
        }
        socket.to(data.chat_id).to(data.agent_handle).emit('return-message-whatsapp', data);
    });

    socket.on('return-message-whatsapp', (data) => {
        socket.to(data.chat_id).to(data.agent_handle).emit('return-message-whatsapp', data); //notused
    });
}