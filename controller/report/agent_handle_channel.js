'use strict';
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const { logger, response } = require('../../helper');

const agent_handle_channel = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { action } = req.body;

        let unixtime_range, timestamp_range = '';
        if (action === 'Today') {
            unixtime_range = 'DATE(from_unixtime(a.start_time)) = DATE(NOW())';
            timestamp_range = 'DATE(date_create) = DATE(NOW())';
        }
        else if (action === 'Last7Days') {
            unixtime_range = 'DATE(from_unixtime(a.start_time)) BETWEEN DATE_SUB(DATE(NOW()), INTERVAL 7 DAY) AND DATE(NOW())';
            timestamp_range = 'DATE(date_create) BETWEEN DATE_SUB(DATE(NOW()), INTERVAL 7 DAY) AND DATE(NOW())';
        }

        const result = await knex.raw(`
            SELECT agent_handle, channel, 
                COUNT(*) AS total_handle, 
                SEC_TO_TIME(SUM(handling_time)) AS total_time, 
                SEC_TO_TIME(MIN(handling_time)) AS min_time, 
                SEC_TO_TIME(MAX(handling_time)) AS max_time, 
                SEC_TO_TIME(ROUND(AVG(handling_time))) AS avg_time
            FROM (
                SELECT b.agent_handle, 'Call' AS channel, 
                    a.ended_time - a.start_time AS handling_time
                FROM call_detail_records a
                JOIN call_history b ON a.session_id = b.interaction_id
                WHERE a.answered_time <> 0 AND ${unixtime_range}
                UNION ALL
                SELECT agent_handle, SUBSTRING_INDEX(channel, '_', 1) AS channel, 
                    (SELECT date_create FROM chats b WHERE b.chat_id=chats.chat_id ORDER BY date_create DESC LIMIT 1) - (SELECT date_create FROM chats b WHERE b.chat_id=chats.chat_id ORDER BY date_create ASC LIMIT 1) AS handling_time
                FROM chats 
                WHERE flag_end='Y' AND agent_handle <> 'SPAM' AND ${timestamp_range}
                GROUP BY chat_id, agent_handle, channel
            ) AS cdr_summary
            GROUP BY agent_handle, channel ORDER BY total_handle DESC
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        logger('report/agent_handle_channel', error);
        res.status(500).end();
    }
}

const data_channel_chart = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { action } = req.body;

        let unixtime_range, timestamp_range = '';
        if (action === 'Today') {
            unixtime_range = 'DATE(from_unixtime(a.start_time)) = DATE(NOW())';
            timestamp_range = 'DATE(date_create) = DATE(NOW())';
        }
        else if (action === 'Last7Days') {
            unixtime_range = 'DATE(from_unixtime(a.start_time)) BETWEEN DATE_SUB(DATE(NOW()), INTERVAL 7 DAY) AND DATE(NOW())';
            timestamp_range = 'DATE(date_create) BETWEEN DATE_SUB(DATE(NOW()), INTERVAL 7 DAY) AND DATE(NOW())';
        }

        const result = await knex.raw(`
            SELECT channel, COUNT(*) AS total_handle
            FROM (
                SELECT b.agent_handle, 'Call' AS channel FROM call_detail_records a
                JOIN call_history b ON a.session_id = b.interaction_id
                WHERE a.answered_time <> 0 AND ${unixtime_range}
                UNION ALL
                SELECT agent_handle, SUBSTRING_INDEX(channel, '_', 1) AS channel FROM chats 
                WHERE flag_end='Y' AND agent_handle <> 'SPAM' AND ${timestamp_range}
                GROUP BY chat_id, agent_handle, channel
            ) AS cdr_summary
            GROUP BY channel ORDER BY total_handle DESC
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        logger('report/data_channel_chart', error);
        res.status(500).end();
    }
}



module.exports = { agent_handle_channel, data_channel_chart }