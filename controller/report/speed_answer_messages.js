'use strict';
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const { logger, response } = require('../../helper');

const speed_answer_messages = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { action } = req.body;

        let date_range = '';
        if (action === 'Today') {
            date_range = 'DATE(dateCreate) = DATE(NOW())';
        }
        else if (action === 'Last7Days') {
            date_range = 'DATE(dateCreate) BETWEEN DATE_SUB(DATE(NOW()), INTERVAL 7 DAY) AND DATE(NOW())';
        }

        const result = await knex.raw(`
            SELECT
                time_range,
                total_count,
                SUM( total_count ) OVER () AS total_sum,
                ( total_count / SUM( total_count ) OVER () * 100 ) AS percentage 
            FROM (
                SELECT '1' AS ROW, '5 second' AS time_range,COUNT(*) AS total_count 
                FROM v_chat_interaction 
                WHERE ( dateAnswer - dateAssign ) >= 0 
                AND ( dateAnswer - dateAssign ) <= 5 UNION ALL SELECT '2' AS ROW, '10 second' AS time_range, COUNT(*) AS total_count FROM v_chat_interaction WHERE ( dateAnswer - dateAssign ) >= 5 AND ${date_range} 
                AND ( dateAnswer - dateAssign ) <= 10 UNION ALL SELECT '3' AS ROW, '20 second' AS time_range, COUNT(*) AS total_count FROM v_chat_interaction WHERE ( dateAnswer - dateAssign ) >= 10  AND ${date_range}
                AND ( dateAnswer - dateAssign ) <= 20 UNION ALL SELECT '4' AS ROW, '30 second' AS time_range, COUNT(*) AS total_count FROM v_chat_interaction WHERE ( dateAnswer - dateAssign ) >= 20  AND ${date_range}  
                AND ( dateAnswer - dateAssign ) <= 30 UNION ALL SELECT '5' AS ROW, '40 second' AS time_range, COUNT(*) AS total_count FROM v_chat_interaction WHERE ( dateAnswer - dateAssign ) >= 30  AND ${date_range}
                AND ( dateAnswer - dateAssign ) <= 40 UNION ALL SELECT '6' AS ROW, '50 second' AS time_range, COUNT(*) AS total_count FROM v_chat_interaction WHERE ( dateAnswer - dateAssign ) >= 40  AND ${date_range} 
                AND ( dateAnswer - dateAssign ) <= 50 UNION ALL SELECT '7' AS ROW, '60 second' AS time_range, COUNT(*) AS total_count FROM v_chat_interaction WHERE ( dateAnswer - dateAssign ) >= 50  AND ${date_range} 
                AND ( dateAnswer - dateAssign ) <= 60 UNION ALL SELECT '8' AS ROW, '60 second+' AS time_range, COUNT(*) AS total_count FROM v_chat_interaction WHERE ( dateAnswer - dateAssign ) > 60  AND ${date_range}
            ) subquery 
            ORDER BY ROW
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        logger('report/speed_answer_messages', error);
        res.status(500).end();
    }
}

module.exports = { speed_answer_messages }