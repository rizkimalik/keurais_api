'use strict';
const knex = require('../../config/db_connect');
const logger = require('../../helper/logger');
const response = require('../../helper/json_response');

const agent_online = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const users = await knex('users').where({ login: 1, user_level: 'L1' }).orderBy('username', 'asc');
        for (let i = 0; i < users.length; i++) {
            let view_chat = `SELECT chat_id,agent_handle FROM view_chats WHERE flag_to='customer' AND agent_handle='${users[i].username}' AND CONVERT(date_create, DATE)=CONVERT(CURRENT_DATE(), DATE) GROUP BY chat_id,agent_handle`;
            let total = await knex.raw(`SELECT COUNT(chat_id) as total_handle FROM (${view_chat}) as view_chat`);
            let aux = await knex('aux_status').select('aux_name').where({ id: users[i].aux }).first();

            users[i].total_handle = total[0][0].total_handle;
            users[i].aux_name = aux.aux_name;
        }
        response.ok(res, users);
    }
    catch (error) {
        logger('dash_sosmed/agent_online', error);
        res.status(500).end();
    }
}

const total_channel = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const channel = await knex('channels').select('channel').where('active', 1).groupBy('channel');
        for (let i = 0; i < channel.length; i++) {
            if (channel[i].channel === 'Call') {
                let total_channel = await knex.raw(`SELECT COUNT(interaction_id) as total FROM call_history WHERE CONVERT(datetime_start, DATE)=CONVERT(CURRENT_DATE(), DATE)`);
                channel[i].total = total_channel[0][0].total;
                channel[i].queing = 0;
            }
            else if (channel[i].channel === 'Email') {
                let total_channel = await knex.raw(`SELECT COUNT(email_id) as total FROM email_mailbox WHERE CONVERT(date_email, DATE)=CONVERT(CURRENT_DATE(), DATE)`);
                let queing_channel = await knex.raw(`SELECT COUNT(email_id) as queing FROM email_mailbox WHERE CONVERT(date_email, DATE)=CONVERT(CURRENT_DATE(), DATE) AND (agent_handle = '' OR agent_handle IS NULL)`);
                channel[i].total = total_channel[0][0].total;
                channel[i].queing = queing_channel[0][0].queing;
            }
            else {
                let view_total = `SELECT chat_id,SUBSTRING_INDEX(channel, '_', 1) AS channel FROM view_chats WHERE flag_to='customer' AND SUBSTRING_INDEX(channel, '_', 1)='${channel[i].channel}' AND agent_handle!='SPAM' AND CONVERT(date_create, DATE)=CONVERT(CURRENT_DATE(), DATE) GROUP BY chat_id,channel`;
                let view_queing = `SELECT chat_id,SUBSTRING_INDEX(channel, '_', 1) AS channel FROM view_chats WHERE flag_to='customer' AND SUBSTRING_INDEX(channel, '_', 1)='${channel[i].channel}' AND (agent_handle='' OR agent_handle IS NULL) AND CONVERT(date_create, DATE)=CONVERT(CURRENT_DATE(), DATE) GROUP BY chat_id,channel`;
                let total_channel = await knex.raw(`SELECT COUNT(chat_id) as total FROM (${view_total}) as view_chat`);
                let queing_channel = await knex.raw(`SELECT COUNT(chat_id) as queing FROM (${view_queing}) as view_chat`);

                channel[i].total = total_channel[0][0].total;
                channel[i].queing = queing_channel[0][0].queing;
            }

        }
        response.ok(res, channel);
    }
    catch (error) {
        logger('dash_sosmed/total_channel', error);
        res.status(500).end();
    }
}

const grafik_channel = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const channel = await knex('channels').select('channel').where('active', 1).groupBy('channel');
        for (let i = 0; i < channel.length; i++) {
            let view_chat = `SELECT chat_id,agent_handle,channel FROM view_chats WHERE flag_to='customer' and channel='${channel[i].channel}' AND agent_handle!='SPAM' AND CONVERT(date_create, DATE)=CONVERT(CURRENT_DATE(), DATE) GROUP BY chat_id,agent_handle,channel`;
            let total_channel = await knex.raw(`SELECT COUNT(chat_id) as total FROM (${view_chat}) as view_chat`);
            channel[i].total = total_channel[0][0].total;
        }
        response.ok(res, channel);
    }
    catch (error) {
        logger('dash_sosmed/grafik_channel', error);
        res.status(500).end();
    }
}

const channel_interaction = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY datetime DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT @rownum := @rownum + 1 AS id, a.* FROM v_channel_interactions a, (SELECT @rownum := 0) b
            WHERE CONVERT(a.datetime, DATE)=CONVERT(CURRENT_DATE(), DATE)
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total from v_channel_interactions 
            WHERE CONVERT(datetime, DATE)=CONVERT(CURRENT_DATE(), DATE) ${filtering}
        `);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        logger('dash_sosmed/channel_interaction', error);
        res.status(500).end();
    }
}

module.exports = {
    agent_online,
    total_channel,
    grafik_channel,
    channel_interaction,
}