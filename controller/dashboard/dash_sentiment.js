'use strict';
const knex = require('../../config/db_connect');
const { logger, response } = require('../../helper');

const data_analysis_sentiment = async (req, res) => {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { action } = req.body;

        let date_range = '';
        if (action === 'Today') {
            date_range = 'DATE(date_create) = DATE(NOW())';
        }
        else if (action === 'Week') {
            date_range = 'WEEK(date_create) = WEEK(NOW())';
        }
        else if (action === 'Month') {
            date_range = 'MONTH(date_create) = MONTH(NOW())';
        }

        const result = await knex.raw(`SELECT user_id,name,channel,message,DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create,sentiment,objective_score,positive_score,negative_score FROM view_chats WHERE flag_to='customer' AND agent_handle <> 'SPAM' AND ${date_range} ORDER BY date_create DESC LIMIT 10`);
        response.ok(res, result[0]);
    }
    catch (error) {
        logger('dash_analityc/data_analityc_sentiment', error);
    }
}

const total_by_sentiment = async (req, res) => {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { action } = req.body;

        let date_range = '';
        if (action === 'Today') {
            date_range = 'DATE(date_create) = DATE(NOW())';
        }
        else if (action === 'Week') {
            date_range = 'WEEK(date_create) = WEEK(NOW())';
        }
        else if (action === 'Month') {
            date_range = 'MONTH(date_create) = MONTH(NOW())';
        }

        const result = await knex.raw(`SELECT sentiment, COUNT(sentiment) as total FROM view_chats WHERE flag_to='customer' AND agent_handle <> 'SPAM' AND ${date_range} GROUP BY sentiment ORDER BY total DESC`);
        response.ok(res, result[0]);
    }
    catch (error) {
        logger('dash_analityc/total_by_sentiment', error);
    }
}

const sentiment_by_channel = async (req, res) => {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { action } = req.body;

        let date_range = '';
        if (action === 'Today') {
            date_range = 'DATE(date_create) = DATE(NOW())';
        }
        else if (action === 'Week') {
            date_range = 'WEEK(vc.date_create) = WEEK(NOW())';
        }
        else if (action === 'Month') {
            date_range = 'MONTH(vc.date_create) = MONTH(NOW())';
        }

        const result = await knex.raw(`
            SELECT SUBSTRING_INDEX(vc.channel, '_', 1) AS channel, 
                SUM(CASE WHEN vc.sentiment = 'neutral' THEN 1 ELSE 0 END) AS total_neutral, 
                SUM(CASE WHEN vc.sentiment = 'positive' THEN 1 ELSE 0 END) AS total_positive, 
                SUM(CASE WHEN vc.sentiment = 'negative' THEN 1 ELSE 0 END) AS total_negative
            FROM view_chats vc
            WHERE vc.flag_to = 'customer' AND vc.agent_handle <> 'SPAM' AND ${date_range}
            GROUP BY vc.channel
            ORDER BY vc.channel DESC
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        logger('dash_analityc/sentiment_by_channel', error);
    }
}

const sentiment_by_week = async (req, res) => {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { action } = req.body;

        let date_range = '';
        if (action === 'Today') {
            date_range = 'DATE(date_name) = DATE(NOW())';
        }
        else if (action === 'Week') {
            date_range = 'WEEK(date_create) = WEEK(NOW())';
        }
        else if (action === 'Month') {
            date_range = 'MONTH(date_create) = MONTH(NOW())';
        }

        const result = await knex.raw(`
            SELECT * FROM (
                SELECT 
                    DAYNAME( date_create ) AS day_name,
                    DATE( date_create ) AS date_name,
                    SUM(CASE WHEN sentiment = 'neutral' THEN 1 ELSE 0 END) AS total_neutral, 
                    SUM(CASE WHEN sentiment = 'positive' THEN 1 ELSE 0 END) AS total_positive, 
                    SUM(CASE WHEN sentiment = 'negative' THEN 1 ELSE 0 END) AS total_negative
                FROM view_chats 
                WHERE flag_to='customer' AND agent_handle <> 'SPAM' AND WEEK(date_create)=WEEK(NOW())
                GROUP BY DATE ( date_create ) 
            ) AS t 
            GROUP BY t.day_name
            ORDER BY t.date_name
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        logger('dash_analityc/sentiment_by_week', error);
    }
}

module.exports = {
    data_analysis_sentiment,
    total_by_sentiment,
    sentiment_by_channel,
    sentiment_by_week,
}