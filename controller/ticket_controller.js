'use strict';
const knex = require('../config/db_connect');
const date = require('date-and-time');
const { auth_jwt_bearer } = require('../middleware');
const logger = require('../helper/logger');
const response = require('../helper/json_response');
const { datetime, isostring } = require('../helper/datetime_format');

const index = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const tickets = await knex('view_tickets');
        response.ok(res, tickets);
    }
    catch (error) {
        logger('ticket/index', error);
        res.status(500).end();
    }
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { ticket_number } = req.params;
        const tickets = await knex('view_tickets').where({ ticket_number }).first();
        const ticket_additional = await knex('ticket_business_data').where({ ticket_number: tickets.ticket_number }).first();
        tickets.date_create = isostring(tickets.date_create);

        response.ok(res, { ...tickets, ...ticket_additional });
    }
    catch (error) {
        logger('ticket/show', error);
        res.status(500).end();
    }
}


const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            ticket_number,
            customer_id,
            ticket_source,
            status,
            category_id,
            category_sublv1_id,
            category_sublv2_id,
            category_sublv3_id,
            complaint_detail,
            response_detail,
            sla,
            ticket_position,
            dispatch_department,
            department_id,
            source_information,
            mass_distruption,
            group_ticket_number,
            thread_id, //?chat_id, email_id, interaction_id
            account, //?user_id customer
            subject,
            user_create,
            date_create,
            cust_name,
            cust_email,
            cust_telephone,
            cust_address,

            //? additional data
            business_data1,
            business_data2,
            business_data3,
        } = req.body;
        const no_reference = group_ticket_number === '' ? date.format(new Date(), 'DDHHmmSS') : group_ticket_number; //? auto generate id jika kosong

        const result = await knex.raw(`CALL sp_ticket_insert(
            '${ticket_number}',
            '${customer_id}',
            '${ticket_source}',
            '${status}',
            '${category_id}',
            '${category_sublv1_id}',
            '${category_sublv2_id}',
            '${category_sublv3_id}',
            '${complaint_detail}',
            '${response_detail}',
            '${sla}',
            '${ticket_position}',
            '${dispatch_department}',
            '${department_id}',
            '${source_information}',
            '${mass_distruption}',
            '${no_reference}',
            '${user_create}',
            '${date_create}',
            '${thread_id}',
            '${account}', 
            '${subject}',
            '${cust_name}',
            '${cust_email}',
            '${cust_telephone}',
            '${cust_address}',
            '${business_data1}',
            '${business_data2}',
            '${business_data3}'
        )`);

        response.ok(res, result);
    }
    catch (error) {
        logger('ticket/store', error);
        res.status(500).end();
    }
}

const ticket_last_response = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            ticket_number,
            agent_handle,
        } = req.body;

        const user = await knex('users').where({ username: agent_handle }).first();
        if (user) {
            await knex.raw(`
                UPDATE tickets SET last_response_by='${agent_handle}', last_response_at=NOW() 
                WHERE ticket_number='${ticket_number}' AND (last_response_by='' OR last_response_by IS NULL) AND ticket_position='${user.user_level}' AND dispatch_department='${user.department}'
            `);
            response.ok(res, 'success update ticket_last_response');
        }
        else {
            response.error(res, 'gagal update ticket_last_response', 'ticket/ticket_last_response');
        }
    }
    catch (error) {
        response.error(res, error.message, 'ticket/ticket_last_response');
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            ticket_number,
            group_ticket_number,
            ticket_source,
            thread_id,
            status,
            complaint_detail,
            response_detail,
            dispatch_department,
            user_create,
            ticket_position,
            mass_distruption
        } = req.body;

        await knex.raw(`CALL sp_ticket_update(
            '${ticket_number}',
            '${group_ticket_number}',
            '${ticket_source}',
            '${thread_id}',
            '${status}',
            '${complaint_detail}',
            '${response_detail}',
            '${dispatch_department}',
            '${user_create}',
            '${ticket_position}',
            '${mass_distruption}'
        )`);

        response.ok(res, ticket_number);
    }
    catch (error) {
        logger('ticket/update', error);
        res.status(500).end();
    }
}

const ticket_escalations = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            ticket_number,
            status,
            user_create,
            ticket_source,
            dispatch_department,
            ticket_position,
            response_detail
        } = req.body;

        await knex('tickets')
            .update({
                dispatch_department,
                ticket_position,
                response_detail
            })
            .where({ ticket_number });

        store_ticket_interactions({
            ticket_number,
            response_complaint: response_detail,
            status,
            channel: ticket_source,
            user_create,
            first_create: 'No',
            dispatch_ticket: 'Yes',
            dispatch_to_layer: ticket_position,
            interaction_type: 'Escalation'
        });

        response.ok(res, ticket_number);
    }
    catch (error) {
        logger('ticket/ticket_escalations', error);
        res.status(500).end();
    }
}

const store_ticket_threads = async function (req) {
    try {
        const {
            thread_id,
            thread_channel,
            account,
            subject,
            ticket_number,
            customer_id,
            created_at,
            created_by,
        } = req;

        await knex('ticket_threads')
            .insert([{
                thread_id,
                thread_channel,
                account,
                subject,
                ticket_number,
                customer_id,
                created_at,
                created_by,
            }]);
    }
    catch (error) {
        logger('ticket/store_ticket_threads', error);
    }
}

const store_ticket_interactions = async function (req) {
    try {
        const {
            ticket_number,
            thread_id,
            response_complaint,
            status,
            channel,
            user_create,
            first_create,
            dispatch_ticket,
            dispatch_to_layer,
            interaction_type,
            created_at = knex.fn.now()
        } = req;

        await knex('ticket_interactions')
            .insert([{
                ticket_number,
                thread_id,
                channel,
                response_complaint,
                status,
                user_create,
                created_at,
                first_create,
                dispatch_ticket,
                dispatch_to_layer,
                interaction_type,
            }]);
    }
    catch (error) {
        logger('ticket/store_ticket_interactions', error);
    }
}

const ticket_interactions = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, ticket_number } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT *, DATE_FORMAT(created_at,'%Y-%m-%d %H:%i:%s') AS created_at FROM ticket_interactions 
            WHERE ticket_number='${ticket_number}' ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from ticket_interactions WHERE ticket_number='${ticket_number}' ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        logger('ticket/ticket_interactions', error);
        res.status(500).end();
    }
}

const ticket_mass_distruption = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const tickets = await knex.raw(`
            SELECT group_ticket_number, category_name, category_sublv1_name, category_sublv2_name, category_sublv3_name from view_tickets 
            WHERE mass_distruption='1' AND status <> 'Closed' 
            GROUP BY group_ticket_number, category_name, category_sublv1_name, category_sublv2_name, category_sublv3_name
        `);

        response.ok(res, tickets[0]);
    }
    catch (error) {
        logger('ticket/ticket_mass_distruption', error);
        res.status(500).end();
    }
}

const ticket_reference = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, group_ticket_number } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create FROM view_tickets 
            WHERE group_ticket_number='${group_ticket_number}'
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total from view_tickets 
            WHERE group_ticket_number='${group_ticket_number}' ${filtering}
        `);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        logger('ticket/ticket_reference', error);
        res.status(500).end();
    }
}

const publish = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const now = new Date();
        const {
            customer_id,
            group_ticket_number = date.format(now, 'DDHHmmSS'),
        } = req.body;

        await knex('tickets').update({ group_ticket_number }).where({ customer_id, group_ticket_number: '' });
        await knex('ticket_closed').update({ group_ticket_number }).where({ customer_id, group_ticket_number: '' });
        response.ok(res, group_ticket_number);
    }
    catch (error) {
        logger('ticket/publish', error);
        res.status(500).end();
    }
}

const data_publish = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { customer_id } = req.params;
        const tickets = await knex('view_tickets').where({ customer_id, group_ticket_number: '' }).orderBy('id', 'desc');

        for (let i = 0; i < tickets.length; i++) {
            tickets[i].date_create = datetime(tickets[i].date_create)
        }
        response.ok(res, tickets);
    }
    catch (error) {
        logger('ticket/data_publish', error);
        res.status(500).end();
    }
}

const history_transaction = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, customer_id } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 5;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create FROM view_tickets 
            WHERE customer_id='${customer_id}' ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from view_tickets WHERE customer_id='${customer_id}' ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        logger('ticket/history_transaction', error);
        res.status(500).end();
    }
}

const ticket_history = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, date_start, date_end, customer_id } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let filter_by_customer = '';
        if (customer_id) {
            filter_by_customer = `AND customer_id='${customer_id}'`;
        }

        const result = await knex.raw(`
            SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create, DATE_FORMAT(last_response_at,'%Y-%m-%d %H:%i:%s') AS last_response_at FROM view_tickets 
            WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}'
            ${filter_by_customer} ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total from view_tickets 
            WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' ${filter_by_customer} ${filtering}
        `);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        logger('ticket/ticket_history', error);
        res.status(500).end();
    }
}

const ticket_history_export = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { date_start, date_end, customer_id } = req.body;

        let filter_by_customer = '';
        if (customer_id) {
            filter_by_customer = `AND customer_id='${customer_id}'`;
        }

        const result = await knex.raw(`
            SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create FROM view_tickets 
            WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}'
            ${filter_by_customer}  ORDER BY id DESC
        `);

        response.ok(res, result[0]);
    }
    catch (error) {
        logger('ticket/ticket_history_export', error);
        res.status(500).end();
    }
}

const ticket_interaction_user = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            interaction_id,
            customer_id,
            channel,
            channel_type,
        } = req.body;

        const result = await knex.raw(`CALL sp_socmed_interaction_detail(
            '${interaction_id}',
            '${customer_id}',
            '${channel}',
            '${channel_type}'
        )`);

        response.ok(res, result[0][0]);
    }
    catch (error) {
        logger('ticket/ticket_interaction_user', error);
        res.status(500).end();
    }
}

module.exports = {
    index,
    show,
    store,
    update,
    publish,
    data_publish,
    ticket_reference,
    ticket_last_response,
    ticket_mass_distruption,
    history_transaction,
    store_ticket_threads,
    ticket_interactions,
    ticket_escalations,
    ticket_history,
    ticket_history_export,
    ticket_interaction_user,
}