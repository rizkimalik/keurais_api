'use strict';
const knex = require('../config/db_connect');
const { auth_jwt_bearer } = require('../middleware');
const { response, logger, random_string } = require('../helper');
const { upload_attachment_file, mime_type_file } = require('../helper/file_manager');

exports.attachment_ticket_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, ticket_number } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 5;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT * FROM ticket_attachments
            WHERE ticket_number='${ticket_number}'
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);

        const total = await knex.raw(`SELECT COUNT(*) AS total from ticket_attachments WHERE ticket_number='${ticket_number}'`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'attachment/attachment_ticket_list');
    }
}

exports.attachment_ticket_upload = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const attachment_file = req.files?.attachment_file;
        const {
            ticket_number,
            created_by,
            description,
        } = req.body;

        if (attachment_file) {
            const filename = random_string(5) + '-' + attachment_file.name;
            await upload_attachment_file({ channel: 'ticket', attachment: attachment_file, filename });
            await knex('ticket_attachments')
                .insert([{
                    ticket_number,
                    description,
                    file_name: filename,
                    file_size: attachment_file.size,
                    file_type: await mime_type_file(attachment_file.name),
                    file_url: `./${process.env.ATTACHMENT_DIR}/ticket/${filename}`,
                    created_by,
                    created_at: knex.fn.now()
                }]);
        }
        response.ok(res, 'attachment_ticket_upload success');
    }
    catch (error) {
        logger('attachment/attachment_ticket_upload', error.message);
    }
}