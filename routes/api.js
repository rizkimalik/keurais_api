'use strict';
const { file_manager } = require('../helper');
const auth = require('../controller/auth_controller');
const aux = require('../controller/aux_controller');
const menu = require('../controller/menu_controller');
const user = require('../controller/users_controller');
const master = require('../controller/master_data');
const customer = require('../controller/customer_controller');
const customer_channel = require('../controller/customer_channel_controller');
const ticket = require('../controller/ticket_controller');
const todolist = require('../controller/todolist_controller');
const report = require('../controller/report');
const attachment = require('../controller/attachment_controller');
const { dash_sosialmedia, dash_ticket, dash_sentiment } = require('../controller/dashboard');
const { sosial_media, chat, email, call, facebook, twitter, instagram, whatsapp, pabx, subcription, whatsapp_business, telegram, broadcasting } = require('../controller/omnichannel');

module.exports = function (app, io) {
    app.route('/').get(function (req, res) {
        res.json({ message: "Application API running! 🤘🚀" });
        res.end();
    });

    //? read file logger
    app.route('/logs/:date').get(function (req, res) {
        try {
            const { date } = req.params;
            const log = file_manager.read_dir_file(`./logs/${date}`);
            res.json(log);
            res.end();
        }
        catch (error) {
            res.json(error.message);
            res.end();
        }
    });

    app.route('/main_menu/:user_level').get(menu.main_menu);
    app.route('/menu').get(menu.menu);
    app.route('/menu_modul/:menu_id').get(menu.menu_modul);
    app.route('/menu_submodul/:menu_modul_id').get(menu.menu_submodul);
    app.route('/menu_access').get(menu.menu_access);
    app.route('/modul_access').get(menu.modul_access);
    app.route('/submodul_access').get(menu.submodul_access);
    app.route('/store_access').post(menu.store_access);
    app.route('/delete_access/:id').delete(menu.delete_access);

    app.prefix('/auth', function (api) {
        api.route('/login').post(auth.login);
        api.route('/logout').post(auth.logout);
        api.route('/user_socket').put(auth.user_socket);
        api.route('/check_auth_expired').post(auth.check_auth_expired);
        api.route('/login_kick').post(auth.login_kick);
    });

    app.prefix('/dashboard', function (api) {
        api.route('/socmed/agent_online').get(dash_sosialmedia.agent_online);
        api.route('/socmed/total_channel').get(dash_sosialmedia.total_channel);
        api.route('/socmed/grafik_channel').get(dash_sosialmedia.grafik_channel);
        api.route('/socmed/channel_interaction').post(dash_sosialmedia.channel_interaction);
        api.route('/ticket/total_ticket').post(dash_ticket.total_ticket);
        api.route('/ticket/data_ticket_sla').post(dash_ticket.data_ticket_sla);
        api.route('/ticket/chart_by_channel').post(dash_ticket.chart_by_channel);
        api.route('/ticket/chart_by_total').post(dash_ticket.chart_by_total);
        api.route('/analityc/data_analysis_sentiment').post(dash_sentiment.data_analysis_sentiment);
        api.route('/analityc/total_by_sentiment').post(dash_sentiment.total_by_sentiment);
        api.route('/analityc/sentiment_by_channel').post(dash_sentiment.sentiment_by_channel);
        api.route('/analityc/sentiment_by_week').post(dash_sentiment.sentiment_by_week);
    });

    app.prefix('/master', function (api) {
        api.route('/status').get(master.status.index);
        api.route('/channel').get(master.channel.index);
        api.route('/channel_type').get(master.channel.channel_type);
        api.route('/user_level').get(master.user_level.index);
    });

    //? route category options
    app.prefix('/category', function (api) {
        api.route('/').get(master.category.index);
        api.route('/show/:category_id').get(master.category.show);
        api.route('/store').post(master.category.store);
        api.route('/update').put(master.category.update);
        api.route('/delete/:category_id').delete(master.category.destroy);
    });
    app.prefix('/categorysublv1', function (api) {
        api.route('/:category_id').get(master.categorysublv1.index);
        api.route('/show/:category_sublv1_id').get(master.categorysublv1.show);
        api.route('/store').post(master.categorysublv1.store);
        api.route('/update').put(master.categorysublv1.update);
        api.route('/delete/:category_sublv1_id').delete(master.categorysublv1.destroy);
    });
    app.prefix('/categorysublv2', function (api) {
        api.route('/:category_sublv1_id').get(master.categorysublv2.index);
        api.route('/show/:category_sublv2_id').get(master.categorysublv2.show);
        api.route('/store').post(master.categorysublv2.store);
        api.route('/update').put(master.categorysublv2.update);
        api.route('/delete/:category_sublv2_id').delete(master.categorysublv2.destroy);
    });
    app.prefix('/categorysublv3', function (api) {
        api.route('/:category_sublv2_id').get(master.categorysublv3.index);
        api.route('/show/:category_sublv3_id').get(master.categorysublv3.show);
        api.route('/store').post(master.categorysublv3.store);
        api.route('/update').put(master.categorysublv3.update);
        api.route('/delete/:category_sublv3_id').delete(master.categorysublv3.destroy);
        api.route('/detail_sublv3').post(master.categorysublv3.detail_sublv3);
        api.route('/assigned_customer_sla').post(master.categorysublv3.assigned_customer_sla);
        api.route('/insert_customer_sla').post(master.categorysublv3.insert_customer_sla);
        api.route('/update_customer_sla').post(master.categorysublv3.update_customer_sla);
        api.route('/delete_customer_sla/:id').delete(master.categorysublv3.delete_customer_sla);
    });

    app.prefix('/organization', function (api) {
        api.route('/').get(master.organization.index);
        api.route('/show/:id').get(master.organization.show);
        api.route('/store').post(master.organization.store);
        api.route('/update').put(master.organization.update);
        api.route('/delete/:id').delete(master.organization.destroy);
    });

    app.prefix('/department', function (api) {
        api.route('/').get(master.department.index);
        api.route('/show/:department_id').get(master.department.show);
        api.route('/store').post(master.department.store);
        api.route('/update').put(master.department.update);
        api.route('/delete/:department_id').delete(master.department.destroy);
    });

    app.prefix('/status', function (api) {
        api.route('/').get(master.status.index);
        api.route('/show/:id').get(master.status.show);
        api.route('/store').post(master.status.store);
        api.route('/update').put(master.status.update);
        api.route('/delete/:id').delete(master.status.destroy);
    });

    app.prefix('/channel', function (api) {
        api.route('/').get(master.channel.index);
        api.route('/show/:id').get(master.channel.show);
        api.route('/store').post(master.channel.store);
        api.route('/update').put(master.channel.update);
        api.route('/delete/:id').delete(master.channel.destroy);
    });

    app.prefix('/priority_scale', function (api) {
        api.route('/').get(master.priority_scale.index);
        api.route('/show/:id').get(master.priority_scale.show);
        api.route('/store').post(master.priority_scale.store);
        api.route('/update').put(master.priority_scale.update);
        api.route('/delete/:id').delete(master.priority_scale.destroy);
    });

    app.prefix('/customer_type', function (api) {
        api.route('/').get(master.customer_type.index);
        api.route('/show/:id').get(master.customer_type.show);
        api.route('/store').post(master.customer_type.store);
        api.route('/update').put(master.customer_type.update);
        api.route('/delete/:id').delete(master.customer_type.destroy);
    });

    app.prefix('/chat_template', function (api) {
        api.route('/').post(master.chat_response_template.index);
        api.route('/show/:id').get(master.chat_response_template.show);
        api.route('/store').post(master.chat_response_template.store);
        api.route('/update').put(master.chat_response_template.update);
        api.route('/delete/:id').delete(master.chat_response_template.destroy);
    });

    app.prefix('/aux', function (api) {
        api.route('/').get(aux.index);
        api.route('/status_aux_user').get(aux.status_aux_user);
        api.route('/update_aux_user').put(aux.update_aux_user);
    });

    app.prefix('/customer', function (api) {
        api.route('/').post(customer.index);
        api.route('/data_registered').post(customer.data_registered);
        api.route('/data_grid').get(customer.data_grid);
        api.route('/show/:customer_id').get(customer.show);
        api.route('/store').post(customer.store);
        api.route('/update').put(customer.update);
        api.route('/delete/:customer_id').delete(customer.destroy);
        api.route('/channel').post(customer_channel.index);
        api.route('/data_channel').post(customer_channel.data_channel_customer);
        api.route('/journey/:customer_id').get(customer.customer_journey);
        api.route('/data_export').get(customer.data_export);
        api.route('/syncronize').post(customer.update_syncronize_customer);
        api.route('/unsyncronize').post(customer.update_unsyncronize_customer);
        api.route('/syncronize_data').post(customer.data_syncronize_customer);
        api.route('/insert_channel').post(customer_channel.add_channel_customer);
        api.route('/update_channel').post(customer_channel.update_channel_customer);
        api.route('/delete_channel').post(customer_channel.delete_channel_customer);
    });

    app.prefix('/user', function (api) {
        api.route('/').get(user.index);
        api.route('/show/:id').get(user.show);
        api.route('/store').post(user.store);
        api.route('/update').put(user.update);
        api.route('/reset_password').put(user.reset_password);
        api.route('/delete/:id').delete(user.destroy);
    });

    
    app.prefix('/attachment', function (api) {
        api.route('/ticket/list').post(attachment.attachment_ticket_list);
        api.route('/ticket/upload').post(attachment.attachment_ticket_upload);
    });

    app.prefix('/ticket', function (api) {
        api.route('/').get(ticket.index);
        api.route('/store').post(ticket.store);
        api.route('/update').put(ticket.update);
        api.route('/show/:ticket_number').get(ticket.show);
        api.route('/publish').post(ticket.publish);
        api.route('/publish/:customer_id').get(ticket.data_publish);
        api.route('/escalation').put(ticket.ticket_escalations);
        api.route('/interaction').post(ticket.ticket_interactions);
        api.route('/transaction').post(ticket.history_transaction);
        api.route('/history').post(ticket.ticket_history);
        api.route('/history_export').post(ticket.ticket_history_export);
        api.route('/reference').post(ticket.ticket_reference);
        api.route('/mass_distruption').post(ticket.ticket_mass_distruption);
        api.route('/interaction_user').post(ticket.ticket_interaction_user);
        api.route('/last_response').post(ticket.ticket_last_response);
    });

    app.prefix('/todolist', function (api) {
        api.route('/total_ticket').post(todolist.total_ticket);
        api.route('/data_ticket').post(todolist.data_ticket);
    });

    app.prefix('/subscription', function (api) {
        api.route('/data_subscription_channel').post(subcription.data_subscription_channel);
        api.route('/insert_subscription_channel').post(subcription.insert_subscription_channel);
        api.route('/update_subscription_channel').post(subcription.update_subscription_channel);
        api.route('/delete_subscription_channel').post(subcription.delete_subscription_channel);
        api.route('/detail_subscription_channel').post(subcription.detail_subscription_channel);
        api.route('/data_assigned_department').post(subcription.data_assigned_department);
        api.route('/insert_assign_department').post(subcription.insert_assign_department);
        api.route('/delete_assigned_department').post(subcription.delete_assigned_department);
        api.route('/whatsapp_scan_qrcode').post(subcription.whatsapp_scan_qrcode);
        api.route('/selenium_autologin').post(subcription.selenium_autologin);
    });

    app.prefix('/omnichannel', function (api) {
        api.route('/join_chat').post(chat.join_chat);
        api.route('/list_inbox_messages').post(sosial_media.list_inbox_messages);
        api.route('/conversation_messages').post(sosial_media.conversation_messages);
        api.route('/conversation_closed').post(sosial_media.conversation_closed);
        api.route('/set_account_spam').post(sosial_media.insert_account_spam);
        api.route('/sosmed_notifications').post(sosial_media.sosmed_notifications);
        api.route('/data_messages').post(sosial_media.history_messages);
        api.route('/interaction_detail').post(sosial_media.interaction_detail);
        api.route('/chat_templates').post(sosial_media.chat_templates);
        api.route('/transfer_assign_agent').post(sosial_media.transfer_assign_agent);
        api.route('/agent_ready_toassign').post(sosial_media.agent_ready_toassign);
        // feeds modul
        api.route('/list_inbox_feeds').post(sosial_media.list_inbox_feeds);
        api.route('/show_detail_feed').post(sosial_media.show_detail_feed);
        api.route('/conversation_feeds').post(sosial_media.conversation_feeds);
        // api.route('/close_conversation_feeds').post(sosial_media.show_detail_feeds);

        api.prefix('/email', function (sub_api) {
            sub_api.route('/email_account').post(email.email_accounts);
            sub_api.route('/email_detail').post(email.email_detail);
            sub_api.route('/data_inbox').post(email.data_email_inbox);
            sub_api.route('/data_sent').post(email.data_email_sent);
            sub_api.route('/data_email_history').post(email.data_email_history);
            sub_api.route('/data_email_interaction').post(email.data_email_interaction);
            sub_api.route('/data_email_spam').post(email.data_email_spam);
            sub_api.route('/insert_email_spam').post(email.insert_email_spam);
            sub_api.route('/update_email_unspam').post(email.update_email_unspam);
            sub_api.route('/insert_email_out').post(email.insert_email_out);
            sub_api.route('/insert_customer').post(email.insert_data_customer);
        });

        api.prefix('/call', function (sub_api) {
            sub_api.route('/data_call').post(call.data_call_history);
            sub_api.route('/total_call').post(call.data_total_call);
            sub_api.route('/pabx_get_token').post(pabx.pabx_get_token);
            sub_api.route('/pabx_get_recording').post(pabx.pabx_get_recording);
            sub_api.route('/pabx_get_extension').get(pabx.pabx_get_extensions);
        });
    });

    app.prefix('/webhook', function (api) {
        api.route('/whatsapp/get_messages').post((req, res) => {
            whatsapp.whatsapp_webhook(req, res, io)
        });
        api.route('/whatsapp_business/get_messages').post((req, res) => {
            whatsapp_business.whatsapp_business_webhook(req, res, io)
        });
        api.route('/facebook/get_token').post(facebook.facebook_get_token);
        api.route('/facebook/get_messenger').post((req, res) => {
            facebook.facebook_get_messenger(req, res, io)
        });
        api.route('/facebook/get_feed').post((req, res) => {
            facebook.facebook_get_feed(req, res, io)
        });
        // api.route('/facebook/get_mention').post(facebook.facebook_mention);
        api.route('/instagram/get_token').post(instagram.instagram_get_token);
        api.route('/instagram/get_messenger').post((req, res) => {
            instagram.instagram_get_messenger(req, res, io)
        });
        api.route('/instagram/get_feed').post((req, res) => {
            instagram.instagram_get_feed(req, res, io)
        });
        api.route('/twitter/get_token').post(twitter.twitter_get_token);
        api.route('/twitter/get_directmessage').post((req, res) => {
            twitter.twitter_get_directmessage(req, res, io)
        });
        api.route('/twitter/get_mention').post((req, res) => {
            twitter.twitter_get_mention(req, res, io)
        });
        api.route('/telegram/get_messages').post((req, res) => {
            telegram.telegram_get_message(req, res, io)
        });
        api.route('/call/get_history').post(call.insert_call_history);
        api.route('/call/get_detail_record').post(call.insert_call_detailrecords);
    });

    app.prefix('/broadcast', function (api) {
        api.route('/whatsapp/sendmessage').post(broadcasting.whatsapp_broadcast_send);
        api.route('/whatsapp/channel_list').post(broadcasting.whatsapp_channel_list);
        api.route('/whatsapp/upload_excel_file').post(broadcasting.upload_excel_file);
        // api.route('/broadcast/datalist').post(broadcasting.whatsapp_broadcast_list);
    });

    app.prefix('/report', function (api) {
        api.route('/sla_grid').post(report.report_sla.report_sla_grid);
        api.route('/sla_export').post(report.report_sla.report_sla_export);
        api.route('/interaction_grid').post(report.report_interaction.report_interaction_grid);
        api.route('/interaction_export').post(report.report_interaction.report_interaction_export);
        api.route('/onprogress_grid').post(report.report_onprogress.report_onprogress_grid);
        api.route('/onprogress_export').post(report.report_onprogress.report_onprogress_export);
        api.route('/by_agent_grid').post(report.report_by_agent.report_by_agent_grid);
        api.route('/by_agent_export').post(report.report_by_agent.report_by_agent_export);
        api.route('/agent_handle_channel').post(report.agent_handle_channel.agent_handle_channel);
        api.route('/data_channel_chart').post(report.agent_handle_channel.data_channel_chart);
        api.route('/speed_answer_call').post(report.speed_answer_call.speed_answer_call);
        api.route('/speed_answer_messages').post(report.speed_answer_messages.speed_answer_messages);
        api.route('/sentiment_analysis').post(report.report_sentiment_analysis.report_sentiment_anaysis_grid);
        api.route('/sentiment_analysis_export').post(report.report_sentiment_analysis.report_sentiment_anaysis_export);
        api.route('/monthly_ticket_grid').post(report.report_monthly_ticket.report_monthly_ticket_grid);
        api.route('/monthly_ticket_export').post(report.report_monthly_ticket.report_monthly_ticket_export);
        api.route('/summary_ticket').post(report.report_summary.report_summary_grid);
        api.route('/summary_ticket_export').post(report.report_summary.report_summary_export);
    });

}
