const log4js = require('log4js')

const logger = (folder, message) => {
    const now = new Date();
    console.log(`${now} : [${folder}] = ${JSON.stringify(message)}`);

    if (!log4js.isConfigured()) {
        log4js.configure({
            appenders: {
                api_log: { 
                    type: "file", 
                    filename: process.env.LOG_NAME, 
                    maxLogSize: process.env.LOG_MAX_SIZE, 
                    backups: process.env.LOG_BACKUP_NUMBER, 
                }
            },
            categories: { 
                default: { appenders: ['api_log'], level: 'debug' } 
            },
        });
    }
    const loggers = log4js.getLogger('api_log');
    loggers.debug(`[${folder}] = ${JSON.stringify(message)}`);
}

module.exports = logger;
